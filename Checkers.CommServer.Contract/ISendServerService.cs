﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Reflection;


namespace Checkers.CommServer.Contract
{
    /// <summary>
    /// Kontrakt dla serwisu WCF odpowiadający za wysyłanie.
    /// </summary>
    [ServiceContract(CallbackContract = typeof(IReceiveServerService))]
    public interface ISendServerService
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(string msg, string sender, string receiver);

        [OperationContract(IsOneWay = true)]    
        void SendBoard(string board, string sender, string receiver);

        [OperationContract(IsOneWay = true)]
        void Start(string Name);

        [OperationContract(IsOneWay = true)]
        void Stop(string Name);
    }
}
