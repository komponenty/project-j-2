﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Reflection;

namespace Checkers.CommServer.Contract
{
    /// <summary>
    /// Kontrakt dla serwisu WCF odpowiadający za odbieranie danych
    /// </summary>
    public interface IReceiveServerService
    {
        [OperationContract(IsOneWay = true)]
        void ReceiveMessage(string msg, string receiver);
        [OperationContract(IsOneWay = true)]
        void ReceiveBoard(string board, string receiver);
        [OperationContract(IsOneWay = true)]
        void SendNames(IList<string> names);
    }
}
