﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.GUIHandler.Contract;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Checkers.CheckersBoard.Contract;
using System.Windows.Threading;

namespace Checkers.GUIHandler.Impl
{
    public class NetworkMethods : INetworkMethods
    {
        public System.Net.IPAddress[] GetLocalIPList()
        {
            System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

            return _IPHostEntry.AddressList;
        }

        public string SerializeBoard(CheckersBoard.Contract.ICheckerBoard board)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();

            bf.Serialize(ms, board);
            ms.Flush();
            ms.Position = 0;

            return Convert.ToBase64String(ms.ToArray());
        }

        public CheckersBoard.Contract.ICheckerBoard DeserializeBoard(string board)
        {
            BinaryFormatter bf = new BinaryFormatter();
            byte[] b = Convert.FromBase64String(board);

            MemoryStream ms = new MemoryStream(b);

            ms.Seek(0, SeekOrigin.Begin);

            ICheckerBoard newBoard = bf.Deserialize(ms) as ICheckerBoard;

            return newBoard;
        }
    }
}
