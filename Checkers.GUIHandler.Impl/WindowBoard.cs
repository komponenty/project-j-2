﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.CheckersBoard.Contract;
using Checkers.GUIHandler.Contract;
using System.Windows;
using System.Windows.Media;

namespace Checkers.GUIHandler.Impl
{
    public class WindowBoard : IWindowBoard
    {
        public event ChosenHandler Chosen;
        public IWindowPiece[,] Pieces { set; get; }
        public IWindowPiece selectedPiece;

        public void SetBoard(ICheckerBoard board)
        {
            Pieces = new WindowPiece[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    #region line0
                    if ((i == 0) && j % 2 != 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 0) && j % 2 == 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line1
                    if ((i == 1) && j % 2 == 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 1) && j % 2 != 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line2
                    if ((i == 2) && j % 2 != 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 2) && j % 2 == 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line3
                    if ((i == 3) && j % 2 == 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 3) && j % 2 != 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line4
                    if ((i == 4) && j % 2 != 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 4) && j % 2 == 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line5
                    if ((i == 5) && j % 2 == 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 5) && j % 2 != 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line6
                    if ((i == 6) && j % 2 != 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 6) && j % 2 == 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                    #region line7
                    if ((i == 7) && j % 2 == 0)
                    {
                        Pieces[j, i] = new WindowPiece(i, j, board.getPiece(board.columnRowToTablePos(i, j))._type, board.getPiece(board.columnRowToTablePos(i, j))._color);
                        Pieces[j, i].ChosenPiece += new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
                    }
                    if ((i == 7) && j % 2 != 0)
                        Pieces[j, i] = new WindowPiece(i, j, PieceType.Empty, PieceColor.Empty);
                    #endregion
                }
            }
        }

        public void WindowBoard_Chosen(int row, int column)
        {
            if (selectedPiece == null)
            {
                selectedPiece = Pieces[column, row];
                selectedPiece.DisplayControl.BorderThickness = new Thickness(2, 2, 2, 2);
                selectedPiece.DisplayControl.BorderBrush = new SolidColorBrush(Colors.Blue);
            }
            else if ((selectedPiece.Row == row && selectedPiece.Column == column) || Chosen == null ||
                Chosen(selectedPiece.Column, selectedPiece.Row, column, row) == false)
            {
               
                {
                    selectedPiece.DisplayControl.BorderThickness = new Thickness(0);
                    selectedPiece = null;
                }
            }
        }

        public void Clear()
        {
            int i, j;

            for (i = 0; i < 8; i++)
                for (j = 0; j < 8; j++)
                    if (Pieces != null)
                        Pieces[i, j].ChosenPiece -= new Checkers.GUIHandler.Contract.ChosenHandlerPiece(WindowBoard_Chosen);
        }
    }
}
