﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.GUIHandler.Contract;
using Checkers.CheckersBoard.Contract;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Checkers.GUIHandler.Impl
{
    public class WindowPiece : IWindowPiece
    {
        public event ChosenHandlerPiece ChosenPiece;

        public int Row { get; set; }
        public int Column { get; set; }

        public Border DisplayControl { set; get; }
        public Image DisplayImage { set; get; }

        public WindowPiece(int row, int column, PieceType type, PieceColor color)
        {
            Row = row;
            Column = column;

            CreateImage(type, color);
            CreateControl();
        }

        public void CreateControl()
        {
            Border border = new Border();

            border.Child = DisplayImage;

            if ((Row + Column) % 2 == 0)
            {
                border.Background = new SolidColorBrush(Colors.Beige);
            }
            else
            {
                border.Background = new SolidColorBrush(Colors.Brown);
            }

            DisplayControl = border;
            DisplayControl.MouseLeftButtonDown += new MouseButtonEventHandler(DisplayControl_MouseLeftButtonDown);
        }

        public void DisplayControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (ChosenPiece != null)
            {
                ChosenPiece(Row, Column);
            }
        }

        public void CreateImage(PieceType type, PieceColor color)
        {
            String imageLink = string.Empty;

            DisplayImage = new Image();

            if (type == PieceType.Empty || color == PieceColor.Empty)
                return;

            if (type == PieceType.Normal && color == PieceColor.Red)
                imageLink = "Resources/Rnormal.png";
            if (type == PieceType.King && color == PieceColor.Red)
                imageLink = "Resources/Rking.png";

            if (type == PieceType.Normal && color == PieceColor.Yellow)
                imageLink = "Resources/Ynormal.png";
            if (type == PieceType.King && color == PieceColor.Yellow)
                imageLink = "Resources/Yking.png";

            BitmapImage bitmap = new BitmapImage(new Uri(imageLink, UriKind.Relative));
            DisplayImage.Source = bitmap;
        }
    }
}
