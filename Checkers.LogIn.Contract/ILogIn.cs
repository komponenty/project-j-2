﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.LogIn.Contract
{
    public interface ILogIn
    {
        bool createAccount(String inUserName, String inPassword);
        bool loggingIn(String inUserName, String inPassword);
        void close();
    }
}
