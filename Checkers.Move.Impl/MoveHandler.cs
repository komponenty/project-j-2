﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Move.Contract;
using Checkers.CheckersBoard.Contract;
using Checkers.CheckersBoard.Impl;

namespace Checkers.Move.Impl
{
    public class MoveHandler : IMoveHandler
    {
        public ICheckerBoard moveHandlerBoard;

        IPiece _color = new Piece(PieceType.Empty, PieceColor.Empty);
        IPiece _enemy = new Piece(PieceType.Empty, PieceColor.Empty);

        public MoveHandler(ICheckerBoard board)
        {
            this.moveHandlerBoard = board;
            if (this.moveHandlerBoard.currentPlayer._color == PieceColor.Yellow)
            {
                this._color._color = PieceColor.Yellow;
                this._enemy._color = PieceColor.Red;
            }
            else
            {
                this._color._color = PieceColor.Red;
                this._enemy._color = PieceColor.Yellow;
            }
        }
        /// <summary>
        /// Metoda do usuwania pionków gdy następuje zbicie
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void clearPiece(int from, int to)
        {
            int fromRow = this.moveHandlerBoard.tablePosToRow(from);
            int fromColumn = this.moveHandlerBoard.tablePosToColumn(from);

            int toRow = this.moveHandlerBoard.tablePosToRow(to);
            int toColumn = this.moveHandlerBoard.tablePosToColumn(to);

            int dirX, dirY;

            if (fromColumn <= toColumn)
                dirX = 1;
            else
                dirX = -1;

            if (fromRow <= toRow)
                dirY = 1;
            else
                dirY = -1;

            int column = fromColumn + dirX;
            int row = fromRow + dirY;

            while (row != toRow && column != toColumn)
            {
                int index = this.moveHandlerBoard.columnRowToTablePos(column, row);
                IPiece piece = this.moveHandlerBoard.getPiece(index);

                if (this.moveHandlerBoard.getPiece(index)._color == PieceColor.Yellow)
                {
                    --this.moveHandlerBoard.yellowPieces;
                }
                else if (this.moveHandlerBoard.getPiece(index)._color == PieceColor.Red)
                {
                    --this.moveHandlerBoard.redPieces;
                }
                this.moveHandlerBoard.getPiece(index).changeType(PieceType.Empty);
                this.moveHandlerBoard.getPiece(index).changeColor(PieceColor.Empty);
                column += dirX;
                row += dirY;
            }

        }
        /// <summary>
        /// Ruch na podstawie listy składającej się z ruchów
        /// </summary>
        /// <param name="moves"></param>
        public void move(IList<IMove> moves)
        {
            foreach (var item in moves)
            {
                this.applyMove(item.from, item.to);
            }
            this.moveHandlerBoard.changeSide();
        }
        /// <summary>
        /// Pojedyńczy ruch
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void move(int from, int to)
        {
            MoveValidator moveValidator = new MoveValidator(this.moveHandlerBoard);
            bool flag = moveValidator.mustAttack();
            this.applyMove(from, to);
            if (!flag)
            {
                this.moveHandlerBoard.changeSide();
            }
            else
            {
                this.moveHandlerBoard.changeSide();
            }
        }
        /// <summary>
        /// Wykonuje ruch na planszy
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void applyMove(int from, int to)
        {
            MoveValidator moveValidator = new MoveValidator(this.moveHandlerBoard);
            if (!moveValidator.isValidMove(from, to))
            {
                throw new Exception("Sorry");
            }

            this.clearPiece(from, to);

            if (to < 4 && this.moveHandlerBoard.getPiece(from)._type == PieceType.Normal && this.moveHandlerBoard.getPiece(from)._color == PieceColor.Yellow) // ? odwrotnie ?
            {
                this.moveHandlerBoard.getPiece(to).changeType(PieceType.King);
                this.moveHandlerBoard.getPiece(to).changeColor(PieceColor.Yellow);
            }

            else if (to > 27 && this.moveHandlerBoard.getPiece(from)._type == PieceType.Normal && this.moveHandlerBoard.getPiece(from)._color == PieceColor.Red) // ? odwrotnie ?
            {
                this.moveHandlerBoard.getPiece(to).changeType(PieceType.King);
                this.moveHandlerBoard.getPiece(to).changeColor(PieceColor.Red);
            }

            else
            {
                this.moveHandlerBoard.getPiece(to)._type = this.moveHandlerBoard.getPiece(from)._type;
                this.moveHandlerBoard.getPiece(to)._color = this.moveHandlerBoard.getPiece(from)._color;
            }

            this.moveHandlerBoard.getPiece(from).changeColor(PieceColor.Empty);
            this.moveHandlerBoard.getPiece(from).changeType(PieceType.Empty);
        }
    }
}
