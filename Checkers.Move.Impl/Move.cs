﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Move.Contract;

namespace Checkers.Move.Impl
{
    /// <summary>
    /// Klasa Move
    /// </summary>
    public class Move : IMove
    {
        public int from { get; set; }
        public int to { get; set; }
        /// <summary>
        /// Konstruktor Move
        /// </summary>
        /// <param name="moveFrom"></param>
        /// <param name="moveTo"></param>
        public Move(int moveFrom, int moveTo)
        {
            this.from = moveFrom;
            this.to = moveTo;
        }
    }
}
