﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Move.Contract;
using Checkers.CheckersBoard.Contract;
using Checkers.CheckersBoard.Impl;

namespace Checkers.Move.Impl
{
    public class MoveValidator : IMoveValidator
    {
        public ICheckerBoard validateBoard;

        public MoveValidator(ICheckerBoard board)
        {
            this.validateBoard = board;
            //this.validateBoard = board.clone();
        }
        /// <summary>
        /// Sprawdza czy można wykonać taki ruch
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public bool isValidMove(int from, int to)
        {
            if (from < 0 || from > 32 || to < 0 || to > 32)
                return false;
            if (this.validateBoard.getPiece(from)._type == PieceType.Empty || !(this.validateBoard.getPiece(to)._type == PieceType.Empty))
                return false;
            if (this.validateBoard.getPiece(from)._color != this.validateBoard.currentPlayer._color)
                return false;

            int fromRow = this.validateBoard.tablePosToRow(from);
            int fromCol = this.validateBoard.tablePosToColumn(from);
            int toRow = this.validateBoard.tablePosToRow(to);
            int toCol = this.validateBoard.tablePosToColumn(to);

            Piece color = new Piece(PieceType.Empty, PieceColor.Empty);
            Piece enemy = new Piece(PieceType.Empty, PieceColor.Empty);

            if (this.validateBoard.getPiece(from)._color == PieceColor.Yellow)
            {
                color._color = PieceColor.Yellow;
                enemy._color = PieceColor.Red;
            }
            else
            {
                color._color = PieceColor.Red;
                enemy._color = PieceColor.Yellow;
            }

            int incX, incY;

            if (fromCol > toCol)
                incX = -1;
            else
                incX = 1;

            if (fromRow > toRow)
                incY = -1;
            else
                incY = 1;

            int x = fromCol + incX;
            int y = fromRow + incY;

            if (this.validateBoard.getPiece(from)._type == PieceType.Normal)
            {
                bool goodDir;

                if (((incY == -1 && color._color == PieceColor.Yellow) ||
                    (incY == 1 && color._color == PieceColor.Red)))
                    goodDir = true;
                else
                    goodDir = false;

                if (x == toCol && y == toRow)
                    return goodDir && !this.mustAttack();

                return goodDir && x + incX == toCol && y + incY == toRow &&
                   (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x, y))._color) == enemy._color;
            }
            else
            {
                while ((x != toCol) && (y != toRow) && (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x, y))._type == PieceType.Empty))
                {
                    x += incX;
                    y += incY;
                }

                if (x == toCol && y == toRow)
                    return !this.mustAttack();

                if ((this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x, y))._color == enemy._color)) //was type
                {
                    x += incX;
                    y += incY;
                    while (x != toCol && y != toRow && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x, y))._type == PieceType.Empty)
                    {
                        x += incX;
                        y += incY;
                    }

                    if (x == toCol && y == toRow)
                        return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Sprawdza czy zadany pionek może kogoś zbić
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public bool mayAttack(int pos)
        {
            if (this.validateBoard.getPiece(pos)._type == PieceType.Empty)
                return false;

            Piece color = new Piece(PieceType.Empty, PieceColor.Empty);
            Piece enemy = new Piece(PieceType.Empty, PieceColor.Empty);

            if (this.validateBoard.getPiece(pos)._color == PieceColor.Yellow)
            {
                color._color = PieceColor.Yellow;
                enemy._color = PieceColor.Red;
            }
            else
            {
                color._color = PieceColor.Red;
                enemy._color = PieceColor.Yellow;
            }

            int x = this.validateBoard.tablePosToColumn(pos);
            int y = this.validateBoard.tablePosToRow(pos);

            if (this.validateBoard.getPiece(pos)._type == PieceType.Normal)
            {
                int i;

                if (color._color == PieceColor.Yellow)
                    i = -1;
                else
                    i = 1;

                if (x < 6 && y + i > 0 && y + i < 7 &&
                    (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x + 1, y + i))._color == enemy._color) &&
                    (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x + 2, y + 2 * i))._type == PieceType.Empty))
                    return true;
                if (x > 1 && y + i > 0 && y + i < 7 &&
                    (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x - 1, y + i))._color == enemy._color) &&
                    (this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(x - 2, y + 2 * i))._type == PieceType.Empty))
                    return true;
            }
            else
            {
                int i, j;

                #region diagonal \v
                i = x + 1;
                j = y + 1;

                while (i < 6 && j < 6 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                {
                    i++;
                    j++;
                }

                if (i < 7 && j < 7 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._color == enemy._color)
                {
                    i++;
                    j++;

                    if (i <= 7 && j <= 7 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                        return true;
                }
                #endregion
                #region diagonal ^\
                i = x - 1;
                j = y - 1;
                while (i > 1 && j > 1 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                {
                    i--;
                    j--;
                }

                if (i > 0 && j > 0 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._color == enemy._color)
                {
                    i--;
                    j--;

                    if (i >= 0 && j >= 0 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                        return true;
                }
                #endregion
                #region diagonal /^
                i = x + 1;
                j = y - 1;
                while (i < 6 && j > 1 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                {
                    i++;
                    j--;
                }

                if (i < 7 && j > 0 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._color == enemy._color)
                {
                    i++;
                    j--;

                    if (i <= 7 && j >= 0 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                        return true;
                }
                #endregion
                #region diagonal v
                i = x - 1;
                j = y + 1;
                while (i > 1 && j < 6 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                {
                    i--;
                    j++;
                }

                if (i > 0 && j < 7 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._color == enemy._color)
                {
                    i--;
                    j++;

                    if (i >= 0 && j <= 7 && this.validateBoard.getPiece(this.validateBoard.columnRowToTablePos(i, j))._type == PieceType.Empty)
                        return true;
                }
                #endregion
            }
            return false;

        }
        /// <summary>
        /// Sprawdza na całej planszy czy jakiś pionek może kogoś zbić i wymusza na nim ten ruch
        /// </summary>
        /// <returns></returns>
        public bool mustAttack()
        {
            for (int i = 0; i < 32; i++)
                if (mayAttack(i) && this.validateBoard.getPiece(i)._color == this.validateBoard.currentPlayer._color)
                    return true;
            return false;
        }
    }
}
