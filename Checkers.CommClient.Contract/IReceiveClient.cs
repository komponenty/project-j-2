﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.CommClient.Contract
{
    /// <summary>
    /// Kontrakt dla klienta do odbierania danych z serwisu
    /// </summary>
    public interface IReceiveClient
    {
        void Start(IReceiveClient rc, string name, string address);
        void Stop(string name);
    }
}
