﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;
using Checkers.CheckersBoard.Contract;
using Checkers.CheckersBoard.Impl;
using Checkers.CommClient.Contract;
using Checkers.CommClient.Impl;
using Checkers.CommServer.Contract;
using Checkers.CommServer.Impl;
using Checkers.Computer.Contract;
using Checkers.Computer.Impl;
using Checkers.GUIHandler.Contract;
using Checkers.GUIHandler.Impl;
using Checkers.LeaderBoard.Contract;
using Checkers.LeaderBoard.Impl;
using Checkers.Move.Contract;
using Checkers.Move.Impl;

namespace Checkers.Infrastructure
{
    public class Configuration : NinjectModule
    {
        public override void Load()
        {
            Bind<ICheckerBoard>().To<CheckerBoard>();
            Bind<IPiece>().To<Piece>().WithConstructorArgument("PieceType", new PieceType()).WithConstructorArgument("PieceColor", new PieceColor());
            Bind<IReceiveClient>().To<ReceiveClient>();
            Bind<ISendServerService>().To<ServerService>();
            Bind<Computer.Contract.IComputer>().To<Computer.Impl.Computer>().WithConstructorArgument("board",new CheckerBoard()).WithConstructorArgument("depth",new Int32());
            Bind<IMoveGenerator>().To<MoveGenerator>().WithConstructorArgument("board", new CheckerBoard()); ;
            Bind<INetworkMethods>().To<NetworkMethods>();
            Bind<IWindowBoard>().To<WindowBoard>();
            Bind<IWindowPiece>().To<WindowPiece>().WithConstructorArgument("row", new int()).WithConstructorArgument("column", new int()).WithConstructorArgument("PieceType", new PieceType()).WithConstructorArgument("PieceColor", new PieceColor());
            Bind<ILeaderBoard>().To<LeaderBoard.Impl.LeaderBoard>();
            Bind<IMove>().To<Move.Impl.Move>().WithConstructorArgument("moveFrom", new int()).WithConstructorArgument("moveTo", new int());
            Bind<IMoveHandler>().To<MoveHandler>().WithConstructorArgument("board",new CheckerBoard());
            Bind<IMoveValidator>().To<MoveValidator>().WithConstructorArgument("board", new CheckerBoard()); ;
        }
    }
}
