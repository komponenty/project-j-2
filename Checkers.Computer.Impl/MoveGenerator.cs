﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Computer.Contract;
using Checkers.CheckersBoard.Contract;
using Checkers.CheckersBoard.Impl;
using Checkers.Move.Contract;
using Checkers.Move.Impl;

namespace Checkers.Computer.Impl
{
    public class MoveGenerator : IMoveGenerator
    {
        private ICheckerBoard moveGeneratorBoard;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="board"></param>
        public MoveGenerator(ICheckerBoard board)
        {
            this.moveGeneratorBoard = board.clone();
        }

        /// <summary>
        /// Generuje listę ataków zwykłych pionków
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="color"></param>
        /// <param name="enemy"></param>
        /// <returns></returns>
        public List<IMove> simpleAttack(int pos, IPiece color, IPiece enemy)
        {
            int x = this.moveGeneratorBoard.tablePosToColumn(pos);
            int y = this.moveGeneratorBoard.tablePosToRow(pos);

            List<IMove> moves = new List<IMove>();
            List<IMove> tempMoves = new List<IMove>();

            int i;
            int enemyPos;
            int nextPos;

            if (color._color == PieceColor.Yellow)
                i = -1;
            else
                i = 1;
            //v
            if (x < 6 && y + i > 0 && y + i < 7)
            {
                enemyPos = this.moveGeneratorBoard.columnRowToTablePos(x + 1, y + i);
                nextPos = this.moveGeneratorBoard.columnRowToTablePos(x + 2, y + 2 * i);

                if (this.moveGeneratorBoard.getPiece(enemyPos)._color == enemy._color && this.moveGeneratorBoard.getPiece(nextPos)._type == PieceType.Empty)
                {
                    tempMoves = simpleAttack(nextPos, color, enemy);
                    moves.Add(new Move.Impl.Move(pos, nextPos));
                }

            }
            if (x > 1 && y + i > 0 && y + i < 7)
            {
                enemyPos = this.moveGeneratorBoard.columnRowToTablePos(x - 1, y + i);
                nextPos = this.moveGeneratorBoard.columnRowToTablePos(x - 2, y + 2 * i);

                if (this.moveGeneratorBoard.getPiece(enemyPos)._color == enemy._color && this.moveGeneratorBoard.getPiece(nextPos)._type == PieceType.Empty)
                {
                    tempMoves = simpleAttack(nextPos, color, enemy);
                    moves.Add(new Move.Impl.Move(pos, nextPos));
                }
            }
            //Checked

            return moves;
        }
        /// <summary>
        /// Generuje Całkowitą liste możliwych ruchów
        /// </summary>
        /// <returns></returns>
        public List<IMove> legalMoves()
        {
            MoveValidator moveValidator = new MoveValidator(this.moveGeneratorBoard);
            Piece color = new Piece(PieceType.Empty, PieceColor.Empty);
            Piece enemy = new Piece(PieceType.Empty, PieceColor.Empty);

            color._color = this.moveGeneratorBoard.currentPlayer._color;

            if (color._color == PieceColor.Yellow)
                enemy._color = PieceColor.Red;
            else
                enemy._color = PieceColor.Yellow;

            if (moveValidator.mustAttack())
            {
                return this.generateAttackMoves(color, enemy);
            }
            else
            {
                return this.generateMoves(color, enemy);
            }
        }

        /// <summary>
        /// Generuję listę wszystkich możliwych ruchów danego gracza
        /// </summary>
        /// <param name="color"></param>
        /// <param name="enemy"></param>
        /// <returns></returns>
        public List<IMove> generateMoves(IPiece color, IPiece enemy)
        {
            List<IMove> list = new List<IMove>();
            for (int moveFrom = 0; moveFrom < 32; moveFrom++)
            {
                if (this.moveGeneratorBoard.getPiece(moveFrom)._type != PieceType.Empty)
                    if (this.moveGeneratorBoard.getPiece(moveFrom)._color == this.moveGeneratorBoard.currentPlayer._color)
                    {
                        int x = this.moveGeneratorBoard.tablePosToColumn(moveFrom);
                        int y = this.moveGeneratorBoard.tablePosToRow(moveFrom);

                        if (this.moveGeneratorBoard.getPiece(moveFrom)._type == PieceType.Normal)
                        {
                            int i;
                            if (color._color == PieceColor.Yellow)
                                i = -1;
                            else
                                i = 1;

                            if (x < 7 && y + i >= 0 && y + i <= 7 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(x + 1, y + i))._type == PieceType.Empty)
                            {
                                List<IMove> list2 = new List<IMove>();
                                list2.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(x + 1, y + i)));
                                list.AddRange(list2);
                            }

                            if (x > 0 && y + i >= 0 && y + i <= 7 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(x - 1, y + i))._type == PieceType.Empty)
                            {
                                List<IMove> list2 = new List<IMove>();
                                list2.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(x - 1, y + i)));
                                list.AddRange(list2);
                            }
                        }
                        else
                        {
                            int i2 = x + 1;
                            int j2 = y + 1;

                            while (i2 <= 7 && j2 <= 7 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i2, j2))._type == PieceType.Empty)
                            {
                                List<IMove> tempMove = new List<IMove>();
                                tempMove.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(i2, j2)));

                                list.AddRange(tempMove);

                                i2++;
                                j2++;
                            }

                            i2 = x - 1;
                            j2 = y - 1;

                            while (i2 >= 0 && j2 >= 0 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i2, j2))._type == PieceType.Empty)
                            {
                                List<IMove> tempMove = new List<IMove>();
                                tempMove.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(i2, j2)));

                                list.AddRange(tempMove);

                                i2--;
                                j2--;
                            }

                            i2 = x + 1;
                            j2 = y - 1;

                            while (i2 <= 7 && j2 >= 0 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i2, j2))._type == PieceType.Empty)
                            {
                                List<IMove> tempMove = new List<IMove>();
                                tempMove.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(i2, j2)));

                                list.AddRange(tempMove);

                                i2++;
                                j2--;
                            }

                            i2 = x - 1;
                            j2 = y + 1;

                            while (i2 >= 0 && j2 <= 7 && this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i2, j2))._type == PieceType.Empty)
                            {
                                List<IMove> tempMove = new List<IMove>();
                                tempMove.Add(new Move.Impl.Move(moveFrom, this.moveGeneratorBoard.columnRowToTablePos(i2, j2)));

                                list.AddRange(tempMove);

                                i2--;
                                j2++;
                            }
                        }
                    }
            }
            return list;
        }
        /// <summary>
        /// Generuje listę możliwych ruchów gdy damka atakuje
        /// </summary>
        /// <param name="lastPos"></param>
        /// <param name="pos"></param>
        /// <param name="dir"></param>
        /// <param name="color"></param>
        /// <param name="enemy"></param>
        /// <returns></returns>
        public List<IMove> kingAttack(List<int> lastPos, int pos, int dir, IPiece color, IPiece enemy)
        {
            List<IMove> tempMoves = new List<IMove>();
            List<IMove> moves = new List<IMove>();

            if (dir != 3)
            {
                tempMoves = this.kingDiagonalAttack(lastPos, pos, color, enemy, 1, 1);
                if (tempMoves.Count != 0)
                    moves.AddRange(tempMoves);
            }
            if (dir != 2)
            {
                tempMoves = this.kingDiagonalAttack(lastPos, pos, color, enemy, -1, -1);
                if (tempMoves.Count != 0)
                    moves.AddRange(tempMoves);
            }
            if (dir != 4)
            {
                tempMoves = this.kingDiagonalAttack(lastPos, pos, color, enemy, 1, -1);
                if (tempMoves.Count != 0)
                    moves.AddRange(tempMoves);
            }
            if (dir != 1)
            {
                tempMoves = this.kingDiagonalAttack(lastPos, pos, color, enemy, -1, 1);
                if (tempMoves.Count != 0)
                    moves.AddRange(tempMoves);
            }
            return moves;
        }
        /// <summary>
        /// Generuje listę zbujających ruchów
        /// </summary>
        /// <param name="color"></param>
        /// <param name="enemy"></param>
        /// <returns></returns>
        public List<IMove> generateAttackMoves(IPiece color, IPiece enemy)
        {
            List<IMove> moves = new List<IMove>();
            List<IMove> tempMoves = new List<IMove>();
            for (int pos = 0; pos < 32; ++pos)
            {
                if (this.moveGeneratorBoard.getPiece(pos)._color == this.moveGeneratorBoard.currentPlayer._color)
                {
                    if (this.moveGeneratorBoard.getPiece(pos)._type == PieceType.Normal)
                    {
                        tempMoves = this.simpleAttack(pos, color, enemy);
                    }
                    else
                    {
                        List<int> lastPos = new List<int>();
                        lastPos.Add(pos);
                        tempMoves = this.kingAttack(lastPos, pos, 0, color, enemy);
                    }
                    if (tempMoves.Count != 0)
                    {
                        moves.AddRange(tempMoves);
                    }
                }
            }
            return moves;
        }
        /// <summary>
        /// Generuję listę combo zbijania damki
        /// </summary>
        /// <param name="lastPos"></param>
        /// <param name="pos"></param>
        /// <param name="color"></param>
        /// <param name="enemy"></param>
        /// <param name="incX"></param>
        /// <param name="incY"></param>
        /// <returns></returns>
        public List<IMove> kingDiagonalAttack(List<int> lastPos, int pos, IPiece color, IPiece enemy, int incX, int incY)
        {
            int x = this.moveGeneratorBoard.tablePosToColumn(pos);
            int y = this.moveGeneratorBoard.tablePosToRow(pos);

            List<int> tempPos = new List<int>();

            List<IMove> moves = new List<IMove>();
            List<IMove> tempMoves = new List<IMove>();

            int startPos = lastPos[0];

            int i = x + incX;
            int j = y + incY;

            while (i > 0 && i < 7 && j > 0 && j < 7 &&
                (this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i, j))._type == PieceType.Empty || this.moveGeneratorBoard.columnRowToTablePos(i, j) == startPos))
            {
                i += incX;
                j += incY;
            }
            if (i > 0 && i < 7 && j > 0 && j < 7 && (this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i, j))._color == enemy._color && !lastPos.Contains(this.moveGeneratorBoard.columnRowToTablePos(i, j))))
            {
                lastPos.Add(this.moveGeneratorBoard.columnRowToTablePos(i, j));
                i += incX;
                j += incY;

                int saveI = i;
                int saveJ = j;

                while (i > 0 && i < 7 && j > 0 && j < 7 && (this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i, j))._type == PieceType.Empty || this.moveGeneratorBoard.columnRowToTablePos(i, j) == startPos))
                {
                    int dir;

                    if (incX == 1 && incY == 1)
                        dir = 2;
                    else if (incX == -1 && incY == -1)
                        dir = 3;
                    else if (incX == -1 && incY == 1)
                        dir = 4;
                    else
                        dir = 1;

                    lastPos.ForEach((item) =>
                        {
                            tempPos.Add(item);
                        });
                    tempMoves = kingAttack(tempPos, this.moveGeneratorBoard.columnRowToTablePos(i, j), dir, color, enemy);
                    if (moves.Count != 0)
                    {
                        moves.AddRange(tempMoves);
                    }
                    i += incX;
                    j += incY;
                }
                lastPos.RemoveAt(lastPos.Count - 1);
                if (moves.Count == 0)
                {
                    i = saveI;
                    j = saveJ;
                    while (i > 0 && i < 7 && j > 0 && j < 7 && 
                        (this.moveGeneratorBoard.getPiece(this.moveGeneratorBoard.columnRowToTablePos(i, j))._type == PieceType.Empty || this.moveGeneratorBoard.columnRowToTablePos(i, j) == startPos))
                    {
                        tempMoves = new List<IMove>();
                        tempMoves.Add(new Move.Impl.Move(pos, this.moveGeneratorBoard.columnRowToTablePos(i, j)));
                        moves.AddRange(tempMoves);
                        i += incX;
                        j += incY;
                    }

                }
            }
            return moves;
        }
    }
}
