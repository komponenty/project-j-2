﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Computer.Contract;
using Checkers.CheckersBoard.Contract;
using Checkers.CheckersBoard.Impl;
using Checkers.Move.Contract;
using Checkers.Move.Impl;
using System.Diagnostics;

namespace Checkers.Computer.Impl
{
    public class Computer : IComputer
    {
        private ICheckerBoard currentBoard;
        private IPiece color;
        //private IMoveHandler moveHandler;
        private int maxDepth; // było 1
        /// <summary>
        /// Tablela wag poszczególnych miejsc na planszy;
        /// </summary>
        #region tableWeight
        private int[] tableWeight = { 4, 4, 4, 4, 
                                                  4, 3, 3, 3,
                                                  3, 2, 2, 4,
                                                  4, 2, 1, 3,
                                                  3, 1, 2, 4,
                                                  4, 2, 2, 3,
                                                  3, 3, 3, 4,
                                                  4, 4, 4, 4};
        #endregion
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="gameBoard"></param>
        public Computer(ICheckerBoard gameBoard, int depth)
        {
            this.currentBoard = gameBoard;
            this.maxDepth = depth;
            this.color = new Piece(PieceType.Empty,PieceColor.Yellow);
        }
        /// <summary>
        /// Metora która uruchamia obliczanie ruchów
        /// </summary>
        public void play()
        {
            IMoveHandler moveHandler;
            moveHandler = new MoveHandler(this.currentBoard);
            List<IMove> list = this.minimax(this.currentBoard);
            if (list == null)
                return;
            moveHandler.move(list);
        }
        /// <summary>
        /// Implementacja algorytmu Min-Max dla wyboru najleszpego ruchu
        /// </summary>
        /// <param name="board"></param>
        /// <returns>
        /// Zwraca listę ruchów
        /// </returns>
        public List<IMove> minimax(ICheckerBoard board)
        {
            List<IMove> successors = new List<IMove>();
            List<IMove> bestMove = new List<IMove>();
            MoveGenerator moveGenerator = new MoveGenerator(board);

            int alpha = int.MinValue;

            successors = moveGenerator.legalMoves();

            while (this.mayPlay(successors))
            {
                ICheckerBoard nextBoard;
                IMoveHandler nextBoardHandler;

                List<IMove> moves = new List<IMove>();
                moves.Add(successors[0]);
                successors.RemoveAt(0);

                nextBoard = (CheckerBoard)board.clone();
                nextBoardHandler = new MoveHandler(nextBoard);

                nextBoardHandler.move(moves);

                int value = this.minMove(nextBoard, 1, alpha, int.MaxValue);
                if (value > alpha)
                {
                    alpha = value;
                    bestMove = moves;
                }
            }
            return bestMove;
        }
        /// <summary>
        /// Sprawdza czy lista ruchów coś zawiera
        /// </summary>
        /// <param name="moves"></param>
        /// <returns></returns>
        public bool mayPlay(List<IMove> moves)
        {
            return !(moves.Count == 0) && !(moves[0] == null);
        }
        /// <summary>
        /// Oblicza ruch z punktu widzenia gracza Max
        /// </summary>
        /// <param name="board"></param>
        /// <param name="depth"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <returns></returns>
        public int maxMove(ICheckerBoard board, int depth, int alpha, int beta)
        {
            if (this.cutOff(board, depth))
                return this.eval(board);

            MoveGenerator moveGenerator = new MoveGenerator(board);

            List<IMove> succesors = moveGenerator.legalMoves();

            while (this.mayPlay(succesors))
            {
                ICheckerBoard nextBoard; 
                IMoveHandler nextBoardHandler; 

                List<IMove> moves = new List<IMove>();
                moves.Add(succesors[0]);
                succesors.RemoveAt(0);

                nextBoard = (CheckerBoard)board.clone(); 
                nextBoardHandler = new MoveHandler(nextBoard);
                nextBoardHandler.move(moves);
                int value = this.minMove(nextBoard, depth + 1, alpha, beta);

                if (value > alpha)
                    alpha = value;
                if (alpha > beta)
                    return beta;
            }
            return alpha;
        }
        /// <summary>
        /// Oblicza ruch z punktu widzenia gracza Min
        /// </summary>
        /// <param name="board"></param>
        /// <param name="depth"></param>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <returns></returns>
        public int minMove(ICheckerBoard board, int depth, int alpha, int beta)
        {
            if (this.cutOff(board, depth))
                return this.eval(board);

            MoveGenerator moveGenerator = new MoveGenerator(board);

            List<IMove> successors = moveGenerator.legalMoves();

            while (this.mayPlay(successors))
            {
                ICheckerBoard nextBoard;
                IMoveHandler nextBoardHandler; 

                List<IMove> moves = new List<IMove>();
                moves.Add(successors[0]);
                successors.RemoveAt(0);

                nextBoard = (CheckerBoard)board.clone();
                nextBoardHandler = new MoveHandler(nextBoard);

                nextBoardHandler.move(moves);
                int value = this.maxMove(nextBoard, depth + 1, alpha, beta);

                if (value < beta)
                    beta = value;
                if (beta < alpha)
                    return alpha;
            }
            return beta;
        }
        /// <summary>
        /// Oblicza wartość sił aktualnego gracza 
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public int eval(ICheckerBoard board)
        {
            int colorForce = 0;
            int enemyForce = 0;
            Piece piece = new Piece(PieceType.Empty, PieceColor.Empty);
            for (int pos = 0; pos < 32; pos++)
            {
                piece = (Piece)board.getPiece(pos);
                if (piece._type != PieceType.Empty)
                {
                    if (piece._color == this.color._color)
                        colorForce += this.evalValue(piece, pos);
                    else
                        enemyForce += this.evalValue(piece, pos);
                }
            }

            int evaluation = colorForce - enemyForce;
            return evaluation;
        }
        /// <summary>
        /// Oblicza wartość konkretnego pionka na planszy
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int evalValue(IPiece piece, int pos)
        {
            int value;
            if (piece._color == PieceColor.Yellow)
            {
                if (pos < 4 || pos > 7)
                    value = 5;
                else
                    value = 7;
            }
            else if (piece._color == PieceColor.Red)
            {
                if (pos < 24 || pos > 27)
                    value = 5;
                else
                    value = 7;
            }
            else
                value = 10;

            return value * this.tableWeight[pos];
        }
        /// <summary>
        /// Odcinannie alfa-beta
        /// </summary>
        /// <param name="board"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public bool cutOff(ICheckerBoard board, int depth)
        {
            return depth > this.maxDepth || board.hasEnded();
        }
    }
}
