﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Checkers.CommServer.Contract;
using Checkers.CommServer.Impl;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;

namespace Checkers.CommServer.Host
{
    public class Program
    {
        static ServiceHost host;
        static ServerService cs = new ServerService();

        public Program(string address)
        {
            Uri tcpAdrs = new Uri("net.tcp://" + address + ":7886/Server/ServerService");

            Uri[] baseAddress = { tcpAdrs };

            host = new ServiceHost(typeof(Checkers.CommServer.Impl.ServerService), baseAddress);

            NetTcpBinding binding1 = new NetTcpBinding();
            binding1.Security.Mode = SecurityMode.None;
            binding1.Security.Message.ClientCredentialType = MessageCredentialType.UserName;

            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = false;

            host.Description.Behaviors.Add(smb);

            host.AddServiceEndpoint(typeof(Checkers.CommServer.Contract.ISendServerService), binding1, "tcp");
            Binding mexBinding = MetadataExchangeBindings.CreateMexTcpBinding();
            host.AddServiceEndpoint(typeof(IMetadataExchange), mexBinding, "net.tcp://" + address + ":7887/Server/ServerService/mex");

            host.Open();
        }

        public static void stop()
        {
            host.Close();
            cs.Close();
        }

        static void Main(string[] args)
        {
            //ServiceHost host;
            //ServerService cs = new ServerService();
            //string address1 = "127.0.0.1";

            //Uri tcpAdrs = new Uri("net.tcp://" + address1 + ":7886/Server/ServerService");

            //Uri[] baseAddress = { tcpAdrs };

            //host = new ServiceHost(typeof(Checkers.CommServer.Impl.ServerService));

            Console.WriteLine("Serwer Wystartował");
            Console.ReadKey();
        }
    }
}
