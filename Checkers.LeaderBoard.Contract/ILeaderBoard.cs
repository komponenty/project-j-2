﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.LeaderBoard.Contract
{
    public interface IScore { }

    public interface ILeaderBoard
    {
        void XMLSerialization(string username, int movecounter, string time, bool win);
        //List<Score> XMLDeSerialization();
    }
}
