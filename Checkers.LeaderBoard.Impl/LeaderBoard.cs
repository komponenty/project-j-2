﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.LeaderBoard.Contract;
using System.Xml.Serialization;
using System.IO;

namespace Checkers.LeaderBoard.Impl
{
    [Serializable]
    public class Score : IScore
    {
        [XmlElement("Nazwa Uzytkownika")]
        public string userName { get; set; }
        [XmlElement("Liczba Ruchow")]
        public int moveCounter { get; set; }
        [XmlElement("Czas")]
        public string time { get; set; }
        [XmlElement("Czy Wygral")]
        public bool win { get; set; }

        public Score() { }

        public Score(string username, int movecounter, string time, bool win)
        {
            this.userName = username;
            this.moveCounter = movecounter;
            this.time = time;
            this.win = win;
        }
    }

    public class LeaderBoard : ILeaderBoard
    {
        IList<Score> leaderBoard;

        public void XMLSerialization(string username, int movecounter, string time, bool win)
        {
            if (XMLDeSerialization().Count == 0)
                leaderBoard = new List<Score>();
            else
                leaderBoard = XMLDeSerialization();

            leaderBoard.Add(new Score(username, movecounter, time, win));

            XmlSerializer xmlser = new XmlSerializer(typeof(List<Score>));
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter("dane.xml");
                xmlser.Serialize(sw, leaderBoard);
            }
            catch
            {
                throw new Exception();
            }
            finally
            {
                if (null != sw)
                {
                    sw.Dispose();
                }
            }
        }

        public List<Score> XMLDeSerialization()
        {
            List<Score> leaderBoard1;
            IScore score;
            score = new Score();
            leaderBoard1 = new List<Score>();

            if (File.Exists(Path.Combine(".//","dane.xml")))
            {
                StreamReader sr = new StreamReader("dane.xml");
                XmlSerializer xmlser = new XmlSerializer(typeof(List<Score>));
                leaderBoard1 = (List<Score>)xmlser.Deserialize(sr);
                sr.Dispose();
            }
            return leaderBoard1;
        }

    }
}
