﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GridFinal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WindowBoard windowBoard;
        private CheckersBoard checkerBoard;
        private MoveValidator validator;

        public MainWindow()
        {
            
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            checkerBoard = new CheckersBoard();
            windowBoard = new WindowBoard();
            validator = new MoveValidator(checkerBoard);
            validator.Validate();

            

            windowBoard.Chosen += new WindowBoard.ChosenHandler(windowBoard_Chosen);
            setCheckersBoardGrid();
        }

        bool windowBoard_Chosen(int fromRow, int fromColumn, int toRow, int toColumn)
        {
            byte fromSquare = (byte)(fromRow * 16 + fromColumn);
            byte toSquare = (byte)(toRow * 16 + toColumn);

            if (checkerBoard.Pieces[fromSquare].Color == PieceColor.Yellow)
            {
                if (fromColumn > toColumn)
                {
                    byte wybrany = (byte)((fromRow - 1) * 16 + (fromColumn - 1));
                    checkerBoard.Pieces[wybrany].Type = PieceType.None;
                }

                if (fromColumn < toColumn)
                {
                    byte wybrany = (byte)((fromRow - 1) * 16 + (fromColumn + 1));
                    checkerBoard.Pieces[wybrany].Type = PieceType.None;
                }
            }

            if (checkerBoard.Pieces[fromSquare].Color == PieceColor.Red)
            {
                if (fromColumn > toColumn)
                {
                    byte wybrany = (byte)((fromRow + 1) * 16 + (fromColumn - 1));
                    checkerBoard.Pieces[wybrany].Type = PieceType.None;
                }

                if (fromColumn < toColumn)
                {
                    byte wybrany = (byte)((fromRow + 1) * 16 + (fromColumn + 1));
                    checkerBoard.Pieces[wybrany].Type = PieceType.None;
                }
            }




            if (CancelMove(fromSquare) == true)
            {
                return false;
            }

            if (validator.IsMoveIn(fromSquare, toSquare) == false)
            {
                return true;
            }

            checkerBoard = checkerBoard.Move(fromSquare, toSquare);
            validator = new MoveValidator(checkerBoard);
            validator.Validate();
            if (checkerBoard.IsBoardValid() == false)
            {
                checkerBoard = checkerBoard.LastBoard;
                validator = new MoveValidator(checkerBoard);
                validator.Validate();
                return false;
            }

            setCheckersBoardGrid();

            return true;

        }

        private bool CancelMove(byte fromSquare)
        {
            if (checkerBoard.Pieces[fromSquare].Type == PieceType.None || checkerBoard.Pieces[fromSquare].Color != checkerBoard.NowPlays)
            {
                return true;
            }
            return false;
        }

        private void setCheckersBoardGrid()
        {
            int i = 0;
            int j = 0;

            NowPlaysLabel.Content = checkerBoard.NowPlays.ToString();

            windowBoard.SetBoard(checkerBoard);
            CheckersBoardGrid.Children.Clear();
            for (i = 0; i < 8; i++)
                for (j = 0; j < 8; j++)
                    CheckersBoardGrid.Children.Add(windowBoard.Pieces[i, j].DisplayControl);
        }

    }
}
