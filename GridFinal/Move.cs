﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridFinal
{
    class Move
    {
        public byte FromSquare;
        public byte ToSquare;

        public double Score;

        public Move(byte fromSquare, byte toSquare)
        {
            FromSquare = fromSquare;
            ToSquare = toSquare;

            Score = 0;
        }
    }
}
