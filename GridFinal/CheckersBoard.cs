﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridFinal
{
    class CheckersBoard
    {
        public Piece[] Pieces;
        public CheckersBoard LastBoard;
        public PieceColor NowPlays;

        public byte MovedFromSquare;
        public byte MovedToSquare;

        public int RedPieces;
        public int YellowPieces;

        public bool IsRedChecked;
        public bool IsYellowChecked;

        public CheckersBoard()
        {
            byte i, j, line = 0;

            Pieces = new Piece[128];

            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    //line 0
                    if ((i == 0) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Red);
                    if ((i == 0) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Red);
                    //line 1
                    if ((i == 1) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Red);
                    if ((i == 1) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Red);
                    //line 2
                    if ((i == 2) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Red);
                    if ((i == 2) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Red);
                    //line 3
                    if (i == 3)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Red);
                    //line 4
                    if (i == 4)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Red);
                    //line 5
                    if ((i == 5) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Yellow);
                    if ((i == 5) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Yellow);
                    //line 6
                    if ((i == 6) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Yellow);
                    if ((i == 6) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Yellow);
                    //line 7
                    if ((i == 7) && j % 2 != 0)
                        Pieces[line + j] = new Piece(PieceType.Normal, PieceColor.Yellow);
                    if ((i == 7) && j % 2 == 0)
                        Pieces[line + j] = new Piece(PieceType.None, PieceColor.Yellow);
                }
                line += 16;
            }
            LastBoard = null;
            NowPlays = PieceColor.Yellow;

            RedPieces = 12;
            YellowPieces = 12;

            MovedFromSquare = 250;
            MovedToSquare = 250;
        }
        private CheckersBoard(object obj)
        {

        }

        public CheckersBoard Clone()
        {
            byte i, j, line = 0;

            CheckersBoard board = new CheckersBoard(null);

            board.Pieces = new Piece[128];
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                    board.Pieces[line + j] = (Piece)Pieces[line + j].Clone();
                line += 16;
            }

            board.LastBoard = LastBoard;
            board.NowPlays = NowPlays;

            board.MovedFromSquare = MovedFromSquare;
            board.MovedToSquare = MovedToSquare;

            return board;
        }

        public CheckersBoard Move(byte fromSquare, byte toSquare)
        {
            CheckersBoard newBoard = Clone();
            newBoard.Pieces[toSquare] = newBoard.Pieces[fromSquare];
            newBoard.Pieces[fromSquare] = new Piece(PieceType.None, PieceColor.Yellow);

            newBoard.MovedFromSquare = fromSquare;
            newBoard.MovedToSquare = toSquare;

            newBoard.LastBoard = this;

            if (Pieces[toSquare].Type != PieceType.None && Pieces[toSquare].Color == PieceColor.Red)
                newBoard.RedPieces--;
            else if (Pieces[toSquare].Type != PieceType.None && Pieces[toSquare].Color == PieceColor.Yellow)
                newBoard.YellowPieces--;

            if (NowPlays == PieceColor.Red)
                newBoard.NowPlays = PieceColor.Yellow;
            else
                newBoard.NowPlays = PieceColor.Red;

            return newBoard;
        }

        public bool IsBoardValid()
        {
            if (NowPlays == PieceColor.Red && IsYellowChecked == true)
                return false;
            if (NowPlays == PieceColor.Yellow && IsRedChecked == true)
                return false;

            return true;
        }
    }
}
