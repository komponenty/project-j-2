﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridFinal
{
    class Piece
    {
        public PieceType Type;
        public PieceColor Color;
        public int Value;

        public Piece(PieceType type, PieceColor color)
        {
            Type = type;
            Color = color;
            Value = Piece.GetValue(Type);
        }
        public static int GetValue(PieceType type)
        {
            if (type == PieceType.King)
                return 500;
            else if (type == PieceType.Normal)
                return 100;
            return 0;
        }
        public Piece Clone()
        {
            Piece piece = new Piece(Type, Color);
            return piece;
        }
    }
}
