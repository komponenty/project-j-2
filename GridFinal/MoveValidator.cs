﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridFinal
{
    class MoveValidator
    {
        public bool[] YellowAttacks;
        public bool[] RedAttacks;

        private CheckersBoard checkersBoard;
        public LinkedList<Move> ValidMoves;

        public MoveValidator(CheckersBoard board)
        {
            YellowAttacks = new bool[128];
            RedAttacks = new bool[128];

            checkersBoard = board;
            ValidMoves = new LinkedList<Move>();

        }

        public void Validate()
        {
            byte line = 0, i, j;
            Piece tempPiece;

            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    tempPiece = checkersBoard.Pieces[line + j];
                    if (checkersBoard.NowPlays == PieceColor.Yellow)
                        if (tempPiece.Type == PieceType.Normal)
                            ValidateKingYellow((byte)(line + j));
                    if (checkersBoard.NowPlays == PieceColor.Red)
                        if (tempPiece.Type == PieceType.Normal)
                            ValidateKingRed((byte)(line + j));
                }
                line += 16;
            }
        }

        private void ValidateKingYellow(byte fromSquare)
        {
            byte b;

            for (b = (byte)(fromSquare - 17); (b & 0x88) == 0; b -= 17)
            {

                if (checkersBoard.Pieces[b].Type == PieceType.None ||
                    checkersBoard.Pieces[b].Color != checkersBoard.Pieces[fromSquare].Color &&
                    checkersBoard.Pieces[b].Color != PieceColor.Red &&
                    checkersBoard.Pieces[b].Color != PieceColor.Yellow)
                    if (!(fromSquare-b > 18))
                        AddMove(new Move(fromSquare, b));
                    

                if (checkersBoard.Pieces[b].Type == PieceType.None) // zamiast "==" ma być "!="
                    break;
            }

            for (b = (byte)(fromSquare - 15); (b & 0x88) == 0; b -= 15)
            {

                if (checkersBoard.Pieces[b].Type == PieceType.None ||
                    checkersBoard.Pieces[b].Color != checkersBoard.Pieces[fromSquare].Color && checkersBoard.Pieces[b].Color != PieceColor.Red && checkersBoard.Pieces[b].Color != PieceColor.Yellow) // zamiast "==" ma być "!="
                    if (!(fromSquare - b > 18))
                        AddMove(new Move(fromSquare, b));

                if (checkersBoard.Pieces[b].Type == PieceType.None) // zamiast "==" ma być "!="
                    break;
            }


        }

        private void ValidateKingRed(byte fromSquare)
        {
            byte b;

            for (b = (byte)(fromSquare + 15); (b & 0x88) == 0; b += 15)
            {

                if (checkersBoard.Pieces[b].Type == PieceType.None ||
                    checkersBoard.Pieces[b].Color != checkersBoard.Pieces[fromSquare].Color && checkersBoard.Pieces[b].Color != PieceColor.Red && checkersBoard.Pieces[b].Color != PieceColor.Yellow) // zamiast "==" ma być "!="
                    AddMove(new Move(fromSquare, b));

                if (checkersBoard.Pieces[b].Type == PieceType.None) // zamiast "==" ma być "!="
                    break;
            }

            for (b = (byte)(fromSquare + 17); (b & 0x88) == 0; b += 17)
            {
                if (checkersBoard.Pieces[b].Type == PieceType.None ||
                    checkersBoard.Pieces[b].Color != checkersBoard.Pieces[fromSquare].Color && checkersBoard.Pieces[b].Color != PieceColor.Red && checkersBoard.Pieces[b].Color != PieceColor.Yellow) // zamiast "==" ma być "!="
                    AddMove(new Move(fromSquare, b));

                if (checkersBoard.Pieces[b].Type == PieceType.None) // zamiast "==" ma być "!="
                    break;
            }
        }

        private void AddMove(Move move)
        {
            Piece fromPiece = checkersBoard.Pieces[move.FromSquare];
            Piece toPiece = checkersBoard.Pieces[move.ToSquare];

            if (fromPiece.Color == checkersBoard.NowPlays)
                ValidMoves.AddLast(move);
        }

        public bool IsMoveIn(byte fromSquare, byte toSquare)
        {
            return ValidMoves.FirstOrDefault(x => x.FromSquare == fromSquare && x.ToSquare == toSquare) != null;
        }
    }
}
