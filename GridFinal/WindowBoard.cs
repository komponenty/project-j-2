﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace GridFinal
{
    class WindowBoard
    {
        public delegate bool ChosenHandler(int fromRow, int formColumn, int toRow, int toColumn);
        public event ChosenHandler Chosen;

        private WindowPiece selectedPiece;

        private byte moveFromRow;
        private byte moveFromColumn;
        private byte moveToRow;
        private byte moveToColumn;

        public WindowPiece[,] Pieces { private set; get; }

        public void SetBoard(CheckersBoard board)
        {
            byte i, j, line = 0;
            moveFromRow = 250;
            moveFromColumn = 250;
            moveToRow = 250;
            moveToColumn = 250;

            Pieces = new WindowPiece[8, 8];
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    Pieces[i, j] = new WindowPiece(i, j, board.Pieces[line + j].Type, board.Pieces[line + j].Color);
                    Pieces[i, j].Chosen += new WindowPiece.ChosenHandler(WindowsBoard_Chosen);
                    if (board.MovedFromSquare == (line + j) || board.MovedToSquare == (line + j))
                    {
                        Pieces[i, j].DisplayControl.BorderThickness = new Thickness(3, 3, 3, 3);
                        Pieces[i, j].DisplayControl.BorderBrush = new SolidColorBrush(Colors.Black);
                        if (board.MovedFromSquare == (line + j))
                        {
                            moveFromRow = i;
                            moveFromColumn = j;
                        }
                        else
                        {
                            moveToRow = i;
                            moveToColumn = j;
                        }
                    }
                }
                line += 16;
            }
            selectedPiece = null;
        }

        void WindowsBoard_Chosen(int row, int column)
        {
            if (selectedPiece == null)
            {
                selectedPiece = Pieces[row, column];
                selectedPiece.DisplayControl.BorderThickness = new Thickness(2, 2, 2, 2);
                selectedPiece.DisplayControl.BorderBrush = new SolidColorBrush(Colors.Blue);
            }
            else if ((selectedPiece.Row == row && selectedPiece.Column == column) || Chosen == null ||
                Chosen(selectedPiece.Row, selectedPiece.Column, row, column) == false)
            {
                if ((moveFromRow == selectedPiece.Row && moveFromColumn == selectedPiece.Column) ||
                    (moveToRow == selectedPiece.Row && moveToColumn == selectedPiece.Column))
                {
                    selectedPiece.DisplayControl.BorderThickness = new Thickness(3, 3, 3, 3);
                    selectedPiece.DisplayControl.BorderBrush = new SolidColorBrush(Colors.YellowGreen);
                    selectedPiece = null;
                }
                else
                {
                    selectedPiece.DisplayControl.BorderThickness = new Thickness(0);
                    selectedPiece = null;
                }
            }
        }
    }
}
