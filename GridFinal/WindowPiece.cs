﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GridFinal
{
    class WindowPiece
    {
        public delegate void ChosenHandler(int row, int column);
        public event ChosenHandler Chosen;

        public int Row { private set; get; }
        public int Column { private set; get; }

        public Border DisplayControl { private set; get; }
        public Image DisplayImage { private set; get; }

        public WindowPiece(int row, int column, PieceType type, PieceColor color)
        {
            Row = row;
            Column = column;

            CreateImage(type, color);
            CreateControl();       
        }

        private void CreateControl()
        {
            Border border = new Border();

            border.Child = DisplayImage;
            if ((Row + Column) % 2 == 0)
            {
                border.Background = new SolidColorBrush(Colors.Beige);
            }
            else
            {
                border.Background = new SolidColorBrush(Colors.Brown);
            }
            DisplayControl = border;
            DisplayControl.MouseLeftButtonDown += new MouseButtonEventHandler(DisplayControl_MouseLeftButtonDown);
        }

        void DisplayControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Chosen != null)
            {
                Chosen(Row, Column);
            }
        }

        private void CreateImage(PieceType type, PieceColor color)
        {
            string imageLink = String.Empty;

            DisplayImage = new Image();

            if (type == PieceType.None)
                return;

            if (color == PieceColor.Red)
            {
                imageLink = "Resources/R";
            }
            else
            {
                imageLink = "Resources/Y";
            }
            if (type == PieceType.Normal)
            {
                imageLink += "normal.png";
            }
            if (type == PieceType.King)
            {
                imageLink += "king.png";
            }

            BitmapImage bitmap = new BitmapImage(new Uri(imageLink, UriKind.Relative));
            DisplayImage.Source = bitmap;
        }
    }
}
