﻿using Checkers.CheckersBoard.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.GUIHandler.Contract
{
    public interface INetworkMethods
    {
        System.Net.IPAddress[] GetLocalIPList();
        string SerializeBoard(ICheckerBoard board);
        ICheckerBoard DeserializeBoard(string board);
    }
}
