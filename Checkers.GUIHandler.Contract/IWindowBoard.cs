﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Checkers.CheckersBoard.Contract;

namespace Checkers.GUIHandler.Contract
{
    public delegate bool ChosenHandler(int fromRow, int fromColumn, int toRow, int toColumn);

    public interface IWindowBoard
    {
        void SetBoard(ICheckerBoard board);
        void WindowBoard_Chosen(int row, int column);
        IWindowPiece[,] Pieces { set; get; }
        void Clear();
        event ChosenHandler Chosen;

    }
}
