﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Checkers.CheckersBoard.Contract;

namespace Checkers.GUIHandler.Contract
{
    public delegate void ChosenHandlerPiece(int row, int column);

    public interface IWindowPiece
    {
        int Row { get; set; }
        int Column { get; set; }
        void CreateControl();
        void DisplayControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e);
        void CreateImage(PieceType type, PieceColor color);
        event ChosenHandlerPiece ChosenPiece;
        Border DisplayControl { set; get; }
        Image DisplayImage { set; get; }
    }
}
