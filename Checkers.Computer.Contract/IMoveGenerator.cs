﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Move.Contract;
using Checkers.CheckersBoard.Impl;
using Checkers.CheckersBoard.Contract;

namespace Checkers.Computer.Contract
{
    /// <summary>
    /// Generator ruchów dla komputera
    /// </summary>
    public interface IMoveGenerator
    {
        List<IMove> simpleAttack(int pos, IPiece color, IPiece enemy);
        List<IMove> legalMoves();
        List<IMove> generateMoves(IPiece color, IPiece enemy);
        List<IMove> kingAttack(List<int> lastPos, int pos, int dir, IPiece color, IPiece enemy);
        List<IMove> generateAttackMoves(IPiece color, IPiece enemy);
        List<IMove> kingDiagonalAttack(List<int> lastPos, int pos, IPiece color, IPiece enemy, int incX, int incY);
    }
}
