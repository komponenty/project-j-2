﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.Move.Contract;
using Checkers.CheckersBoard.Impl;
using Checkers.CheckersBoard.Contract;

namespace Checkers.Computer.Contract
{
    /// <summary>
    /// Interfejs dla AI
    /// </summary>
    public interface IComputer
    {
        void play();
        List<IMove> minimax(ICheckerBoard board);
        bool mayPlay(List<IMove> moves);
        int maxMove(ICheckerBoard board, int depth, int alpha, int beta);
        int minMove(ICheckerBoard board, int depth, int alpha, int beta);
        int eval(ICheckerBoard board);
        int evalValue(IPiece piece, int pos);
        bool cutOff(ICheckerBoard board, int depth);
    }
}
