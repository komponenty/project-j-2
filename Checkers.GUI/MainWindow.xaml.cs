﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_NewGame_Button_Click(object sender, RoutedEventArgs e)
        {
            NowaGra.NewGame newGameWindow = new NowaGra.NewGame();

            newGameWindow.Left = this.Left;
            newGameWindow.Top = this.Top;
            newGameWindow.Show();
            this.Close(); 
        }

        private void MainWindow_Leaderboard_Button_Click(object sender, RoutedEventArgs e)
        {
            LeaderBoard leadreboardWindow = new LeaderBoard();

            leadreboardWindow.Left = this.Left;
            leadreboardWindow.Top = this.Top;
            leadreboardWindow.Show();
            this.Close(); 
        }

        private void MainWindow_Rules_Button_Click(object sender, RoutedEventArgs e)
        {
            Rules rulesWindow = new Rules();

            rulesWindow.Left = this.Left;
            rulesWindow.Top = this.Top;
            rulesWindow.Show();
            this.Close();
        }

    }
}
