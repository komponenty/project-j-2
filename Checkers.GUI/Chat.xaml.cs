﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.CommClient.Impl;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    public partial class Chat : Window
    {
        ReceiveClient rc = null;
        string myName;

        public Chat()
        {
            InitializeComponent();
        }

        void rc_ReceiveMsg(string sender, string msg)
        {
            if (msg.Length > 0)
                Messages.Text += Environment.NewLine + sender + ">" + msg;
        }

        void rc_NewNames(object sender, List<string> names)
        {
            ListBoxUsers.Items.Clear();
            foreach (string name in names)
            {
                if (name != myName)
                    ListBoxUsers.Items.Add(name);
            }
        }

        private void Send(object sender, RoutedEventArgs e)
        {
            SendMessage();
        }

        private void SendMessage()
        {
            if (ListBoxUsers.Items.Count != 0)
            {
                Messages.Text += Environment.NewLine + myName + ">" + Msg.Text;
                if (ListBoxUsers.SelectedItems.Count ==0)
                {
                    rc.SendMessage(Msg.Text, myName, ListBoxUsers.Items[0].ToString());
                }
                else
                {
                    if (!string.IsNullOrEmpty(ListBoxUsers.SelectedItem.ToString()))
                    {
                        rc.SendMessage(Msg.Text, myName, ListBoxUsers.SelectedItem.ToString());
                    }
                }
            }
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            myName = UserName.Text.Trim();

            rc = new ReceiveClient();
            rc.Start(rc, myName, "192.168.1.13");

            rc.NewNames += new GotNames(rc_NewNames);
            rc.ReceivedMsg += new ReceivedMessage(rc_ReceiveMsg);
        }

        
    }
}
