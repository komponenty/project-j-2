﻿using Checkers.CheckersBoard.Contract;
using Checkers.CommClient.Impl;
using Checkers.CommServer.Host;
using Checkers.Computer.Contract;
using Checkers.GUIHandler.Contract;
using Checkers.Move.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Ninject.Parameters;
using Ninject;
using System.Reflection;
using Checkers.Infrastructure;
using Checkers.LeaderBoard.Contract;


namespace Checkers.GUI.GameBoards
{
    /// <summary>
    /// Interaction logic for GameBoardAI.xaml
    /// </summary>
    public partial class GameBoardAI : Window
    {
        DispatcherTimer dt = new DispatcherTimer();
        Stopwatch stopWatch = new Stopwatch();

        public Program serwer;
        public ReceiveClient receiveClientPlayer;
        public ReceiveClient receiveClientComputer;

        public IMoveHandler moveHandler;
        public IWindowBoard windowBoard;
        public IMoveValidator moveValidator;
        public ICheckerBoard board;
        public INetworkMethods networkMethods;
        public IMoveGenerator generator;
        public IComputer computer;
        public IKernel kernel;
        public ILeaderBoard leaderBoard;

        bool canChange = true;

        string currentTime = string.Empty;
        string myName;
        int totalMoves = 0;

        public GameBoardAI()
        {
            InitializeComponent();
            kernel = new StandardKernel();
            kernel.Load(Assembly.GetAssembly(typeof(Configuration)));

            leaderBoard = kernel.Get<ILeaderBoard>();
            board = kernel.Get<ICheckerBoard>();
            windowBoard = kernel.Get<IWindowBoard>();
            moveValidator = kernel.Get<IMoveValidator>(new ConstructorArgument("board", board));
            moveHandler = kernel.Get<IMoveHandler>(new ConstructorArgument("board", board));
            generator = kernel.Get<IMoveGenerator>(new ConstructorArgument("board", board));
            networkMethods = kernel.Get<INetworkMethods>();
            receiveClientPlayer = new ReceiveClient();
            receiveClientComputer = new ReceiveClient();
            computer = new Computer.Impl.Computer(this.board, NowaGra.NowaGraAI.NewGameAI.depth);

            myName = LogInScreen.userName;


            serwer = new Program("127.0.0.1");
            Thread.Sleep(3000);

            LoginEventPlayer("127.0.0.1");
            Thread.Sleep(1000);
            LoginEventComputer("127.0.0.1");

            windowBoard.Chosen += new Checkers.GUIHandler.Contract.ChosenHandler(windowBoard_Chosen);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Interval = new TimeSpan(0, 0, 0, 0, 1);

            setCheckerBoardGrid();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning)
            {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}",
                    ts.Minutes, ts.Seconds);
                GameBoardAI_Label_Time.Content = currentTime;
            }
        }

        private bool windowBoard_Chosen(int fromRow, int fromColumn, int toRow, int toColumn)
        {
            int fromSquare = board.columnRowToTablePos(fromColumn, fromRow);
            int toSquare = board.columnRowToTablePos(toColumn, toRow);

            if (CancelMove(fromSquare) == true)
                return false;

            if (canChange == false)
                MessageBox.Show("Przeciwnik musi wykonać ruch", "Błąd");

            if (canChange)
            {

                if (moveValidator.mustAttack() == true)
                {
                    if (moveValidator.isValidMove(fromSquare, toSquare))
                    {
                        moveHandler.move(fromSquare, toSquare);

                        this.board.changeSide();
                        setCheckerBoardGrid();
                        bool flag = moveValidator.mayAttack(toSquare);

                        if (flag)
                            return true;
                        else
                        {
                            this.board.changeSide();
                            setCheckerBoardGrid();
                            SendBoardPlayer();
                            canChange = false;
                            Thread.Sleep(1000);
                            computerMove();
                        }
                    }
                }
                else
                {
                    if (moveValidator.isValidMove(fromSquare, toSquare))
                    {
                        moveHandler.move(fromSquare, toSquare);
                        SendBoardPlayer();
                        canChange = false;
                        Thread.Sleep(1000);
                        computerMove();
                    }
                }
            }
            setCheckerBoardGrid();

            return true;
        }

        public void computerMove()
        {
            List<IMove> moves = new List<IMove>();

            moves = computer.minimax(this.board);

            foreach (var item in moves)
            {
                moveHandler.move(item.from, item.to);
            }
            SendBoardComputer();
        }

        private void SendBoardPlayer()
        {
            receiveClientPlayer.SendBoard(networkMethods.SerializeBoard(this.board), myName, "computer");

            totalMoves += 1;
        }

        private void SendBoardComputer()
        {
            receiveClientComputer.SendBoard(networkMethods.SerializeBoard(this.board), "computer", myName);

            totalMoves += 1;
        }



        private void setCheckerBoardGrid()
        {
            windowBoard.SetBoard(board);
            GameBoardAI_CheckersBoardGrid_Grid.Children.Clear();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    GameBoardAI_CheckersBoardGrid_Grid.Children.Add(windowBoard.Pieces[i, j].DisplayControl);
                }
            }

            if (board.hasEnded())
            {
                if (stopWatch.IsRunning)
                    stopWatch.Stop();

                if (this.board.yellowPieces == 0)
                {
                    leaderBoard.XMLSerialization(myName, totalMoves, currentTime, false);
                    MessageBox.Show("Koniec Przegrałeś");
                }
                if (this.board.redPieces == 0)
                {
                    leaderBoard.XMLSerialization(myName, totalMoves, currentTime, true);
                    MessageBox.Show("Koniec Wygrałeś");
                }
            }


            GameBoardAI_Label_PlayerMovesCounter.Content = this.totalMoves;

            moveValidator = kernel.Get<IMoveValidator>(new ConstructorArgument("board", board));
            moveHandler = kernel.Get<IMoveHandler>(new ConstructorArgument("board", board));
        }

        public bool CancelMove(int fromSquare)
        {
            if (board.getPiece(fromSquare)._type == PieceType.Empty)
                return true;

            return false;
        }

        private void GameBoardAI_Button_End_Click(object sender, RoutedEventArgs e)
        {
            receiveClientPlayer.Stop(myName);
            receiveClientComputer.Stop("computer");
            Program.stop();

            NowaGra.NewGame newGameWindow = new NowaGra.NewGame();

            newGameWindow.Left = this.Left;
            newGameWindow.Top = this.Top;
            newGameWindow.Show();
            this.Close();
        }

        private void LoginEventPlayer(string adress)
        {
            receiveClientPlayer.Start(receiveClientPlayer, myName, adress);

            receiveClientPlayer.ReceivedBoards += new ReceivedBoards(rc_receiveBoardPlayer);

            receiveClientPlayer.NewNames += new GotNames(rc_NewNames);
        }

        private void LoginEventComputer(string adress)
        {
            receiveClientComputer.Start(receiveClientComputer, "computer", adress);

            receiveClientComputer.ReceivedBoards += new ReceivedBoards(rc_receiveBoard);

            receiveClientComputer.NewNames += new GotNames(rc_NewNames);
        }

        private void rc_NewNames(object sender, List<string> names)
        {

            dt.Start();
            stopWatch.Reset();
            stopWatch.Start();
        }

        private void rc_receiveBoardPlayer(string sender, string board)
        {
            ICheckerBoard newBoard;
            newBoard = networkMethods.DeserializeBoard(board);

            this.board = newBoard;

            canChange = true;

            setCheckerBoardGrid();
        }

        private void rc_receiveBoard(string sender, string board)
        {
            ICheckerBoard newBoard;
            newBoard = networkMethods.DeserializeBoard(board);

            this.board = newBoard;

            canChange = true;
        }
    }
}
