﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.CommServer.Host;
using Checkers.CommClient.Impl;
using Checkers.Move.Impl;
using Checkers.GUIHandler.Impl;
using Checkers.CheckersBoard.Impl;
using Checkers.CheckersBoard.Contract;
using Checkers.CommClient.Contract;
using Checkers.Move.Contract;
using Checkers.GUIHandler.Contract;
using System.Threading;
using System.Windows.Threading;
using System.Diagnostics;
using Checkers.Computer.Contract;
using Checkers.Computer.Impl;
using Checkers.LeaderBoard.Contract;
using Checkers.LeaderBoard.Impl;
using Ninject.Parameters;
using Ninject;
using System.Reflection;
using Checkers.Infrastructure;

namespace Checkers.GUI.GameBoards
{
    /// <summary>
    /// Interaction logic for GameBoardNet.xaml
    /// </summary>
    public partial class GameBoardNet : Window
    {
        DispatcherTimer dt = new DispatcherTimer();
        Stopwatch stopWatch = new Stopwatch();

        public Program serwer;
        public ReceiveClient receiveClient;

        public ILeaderBoard leaderBoard;
        public IMoveHandler moveHandler;
        public IWindowBoard windowBoard;
        public IMoveValidator moveValidator;
        public ICheckerBoard board;
        public INetworkMethods networkMethods;
        public IMoveGenerator generator;
        public IKernel kernel;

        string oponent;
        string myName;
        string currentTime = string.Empty;

        int totalMoves = 0;
        bool canChange = true;

        public GameBoardNet()
        {
            InitializeComponent();

            kernel = new StandardKernel();
            kernel.Load(Assembly.GetAssembly(typeof(Configuration)));

            leaderBoard = kernel.Get<ILeaderBoard>();
            board = kernel.Get<ICheckerBoard>();
            windowBoard = kernel.Get<IWindowBoard>();
            moveValidator = kernel.Get<IMoveValidator>(new ConstructorArgument("board", board));
            moveHandler = kernel.Get<IMoveHandler>(new ConstructorArgument("board", board));
            generator = kernel.Get<IMoveGenerator>(new ConstructorArgument("board", board));
            networkMethods = kernel.Get<INetworkMethods>();

            receiveClient = new ReceiveClient();
            myName = LogInScreen.userName;

            if (NowaGra.NowaGraNet.NewGameNet.type == "Create")
            {
                serwer = new Program(NowaGra.NowaGraNet.CreateGame.IPadress);
                Thread.Sleep(3000);
                LoginEvent(NowaGra.NowaGraNet.CreateGame.IPadress);
            }
            else
                LoginEvent(NowaGra.NowaGraNet.JoinGame.IPAdress);


            windowBoard.Chosen += new Checkers.GUIHandler.Contract.ChosenHandler(windowBoard_Chosen);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Interval = new TimeSpan(0, 0, 0, 0, 1);

            setCheckerBoardGrid();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning)
            {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}",
                    ts.Minutes, ts.Seconds);
                GameBoardNet_Label_Time.Content = currentTime;
            }
        }

        private bool windowBoard_Chosen(int fromRow, int fromColumn, int toRow, int toColumn)
        {
            int fromSquare = board.columnRowToTablePos(fromColumn, fromRow);
            int toSquare = board.columnRowToTablePos(toColumn, toRow);

            if (CancelMove(fromSquare) == true)
                return false;

            if (canChange == false)
                MessageBox.Show("Przeciwnik musi wykonać ruch", "Błąd");
            if (oponent == null)
            {
                MessageBox.Show("Nie ma przeciwnika", "Błąd");
                return false;
            }

            if (canChange)
            {
                if (moveValidator.mustAttack() == true)
                {
                    if (moveValidator.isValidMove(fromSquare, toSquare))
                    {
                        moveHandler.move(fromSquare, toSquare);

                        this.board.changeSide();
                        setCheckerBoardGrid();
                        bool flag = moveValidator.mayAttack(toSquare);

                        if (flag)
                            return true;
                        else
                        {
                            this.board.changeSide();
                            setCheckerBoardGrid();
                            SendBoard();
                            canChange = false;
                        }
                    }
                }
                else
                {
                    if (moveValidator.isValidMove(fromSquare, toSquare))
                    {
                        moveHandler.move(fromSquare, toSquare);
                        SendBoard();
                        canChange = false;
                    }
                }
            }
            setCheckerBoardGrid();
            return true;
        }

        private void SendBoard()
        {
            receiveClient.SendBoard(networkMethods.SerializeBoard(this.board), myName, oponent);
            totalMoves = totalMoves + 1;
        }

        public bool CancelMove(int fromSquare)
        {
            if (board.getPiece(fromSquare)._type == PieceType.Empty)
                return true;

            return false;
        }

        private void setCheckerBoardGrid()
        {
            windowBoard.Clear();

            windowBoard.SetBoard(board);
            GameBoardNet_CheckersBoardGrid_Grid.Children.Clear();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    GameBoardNet_CheckersBoardGrid_Grid.Children.Add(windowBoard.Pieces[i, j].DisplayControl);
                }
            }


            if (board.hasEnded())
            {
                if (stopWatch.IsRunning)
                    stopWatch.Stop();

                if (NowaGra.NowaGraNet.NewGameNet.type == "Create")
                {
                    if (this.board.yellowPieces == 0)
                    {
                        leaderBoard.XMLSerialization(myName, totalMoves, currentTime, false);
                        MessageBox.Show("Koniec Przegrałeś");
                    }
                    if (this.board.redPieces == 0)
                    {
                        leaderBoard.XMLSerialization(myName, totalMoves, currentTime, true);
                        MessageBox.Show("Koniec Wygrałeś");
                    }
                }
                else
                {
                    if (this.board.redPieces == 0)
                    {
                        leaderBoard.XMLSerialization(myName, totalMoves, currentTime, false);
                        MessageBox.Show("Koniec Przegrałeś");
                    }
                    if (this.board.yellowPieces == 0)
                    {
                        leaderBoard.XMLSerialization(myName, totalMoves, currentTime, true);
                        MessageBox.Show("Koniec Wygrałeś");
                    }
                }
            }

            GameBoardNet_Label_PlayerMovesCounter.Content = this.totalMoves;

            moveValidator = kernel.Get<IMoveValidator>(new ConstructorArgument("board", board));
            moveHandler = kernel.Get<IMoveHandler>(new ConstructorArgument("board", board));
        }

        private void GameBoardNet_Button_Send_Click(object sender, RoutedEventArgs e)
        {
            SendMessage();
        }

        private void SendMessage()
        {
            GameBoardNet_Listbox_MsgView.Text += Environment.NewLine + myName + ">" + GameBoardNet_Textbox_MsgText.Text;
            receiveClient.SendMessage(GameBoardNet_Textbox_MsgText.Text, myName, oponent);
            GameBoardNet_Textbox_MsgText.Clear();
        }

        private void GameBoardNet_Button_End_Click(object sender, RoutedEventArgs e)
        {
            receiveClient.Stop(myName);
            Program.stop();

            NowaGra.NowaGraNet.NewGameNet newGameNetWindow = new NowaGra.NowaGraNet.NewGameNet();

            newGameNetWindow.Left = this.Left;
            newGameNetWindow.Top = this.Top;
            newGameNetWindow.Show();
            this.Close();
        }

        private void LoginEvent(string adress)
        {
            receiveClient.Start(receiveClient, myName, adress);

            receiveClient.ReceivedBoards += new ReceivedBoards(rc_receiveBoard);
            receiveClient.ReceivedMsg += new ReceivedMessage(rc_receiveMsg);

            receiveClient.NewNames += new GotNames(rc_NewNames);
        }

        private void rc_NewNames(object sender, List<string> names)
        {
            foreach (string name in names)
            {
                if (name != myName)
                    this.oponent = name;
            }
            if (oponent != null)
                GameBoardNet_Listbox_MsgView.Text += Environment.NewLine + "Użytkownik " + oponent + " połączył się";

            dt.Start();
            stopWatch.Reset();
            stopWatch.Start();
        }

        private void rc_receiveMsg(string sender, string message)
        {
            if (message.Length > 0)
                GameBoardNet_Listbox_MsgView.Text += Environment.NewLine + sender + ">" + message;
        }

        private void rc_receiveBoard(string sender, string board)
        {
            ICheckerBoard newBoard;
            newBoard = networkMethods.DeserializeBoard(board);
            this.board = newBoard;
            canChange = true;
            totalMoves += 1;
            setCheckerBoardGrid();
        }
    }
}
