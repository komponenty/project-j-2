﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers.GUI.NowaGra
{
    /// <summary>
    /// Interaction logic for NewGame.xaml
    /// </summary>
    public partial class NewGame : Window
    {

        public NewGame()
        {
            InitializeComponent();
        }

        private void NewGame_NewGameAI_Button_Click(object sender, RoutedEventArgs e)
        {
            NowaGraAI.NewGameAI newGameAIWindow = new NowaGraAI.NewGameAI();

            newGameAIWindow.Left = this.Left;
            newGameAIWindow.Top = this.Top;
            newGameAIWindow.Show();
            this.Close();

        }

        private void NewGame_NewGameNet_Button_Click(object sender, RoutedEventArgs e)
        {
            NowaGraNet.NewGameNet newGameNetWindow = new NowaGraNet.NewGameNet();

            newGameNetWindow.Left = this.Left;
            newGameNetWindow.Top = this.Top;
            newGameNetWindow.Show();
            this.Close();
        }

        private void NewGame_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();

            MainWindow.Left = this.Left;
            MainWindow.Top = this.Top;
            MainWindow.Show();
            this.Close();
        }
    }
}
