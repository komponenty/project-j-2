﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers.GUI.NowaGra.NowaGraAI
{
    /// <summary>
    /// Interaction logic for NewGameAI.xaml
    /// </summary>
    public partial class NewGameAI : Window
    {
        public static int depth;

        public NewGameAI()
        {
            InitializeComponent();
        }

        private void NewGameAI_Button_Play_Click(object sender, RoutedEventArgs e)
        {
            GameBoards.GameBoardAI gameBoardAIWindow = new GameBoards.GameBoardAI();

            if (NewGameAI_Radio_Easy.IsChecked == true)
            {
                depth = 1;
            }
            if (NewGameAI_Radio_Hard.IsChecked == true)
            {
                depth = 4;
            }

            gameBoardAIWindow.Left = this.Left;
            gameBoardAIWindow.Top = this.Top;
            gameBoardAIWindow.Show();
            this.Close();
        }

        private void NewGameAI_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            NewGame NewGame = new NewGame();

            NewGame.Left = this.Left;
            NewGame.Top = this.Top;
            NewGame.Show();
            this.Close();
        }
    }
}
