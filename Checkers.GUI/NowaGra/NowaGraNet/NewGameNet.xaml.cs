﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers.GUI.NowaGra.NowaGraNet
{
    /// <summary>
    /// Interaction logic for NewGameNet.xaml
    /// </summary>
    public partial class NewGameNet : Window
    {
        public static String type;

        public NewGameNet()
        {
            InitializeComponent();
        }

        public static string GetGameType()
        {
            return type;
        }

        private void NewGameNet_CreateGame_Button_Click(object sender, RoutedEventArgs e)
        {
            CreateGame createGameWindow = new CreateGame();

            type = "Create";

            createGameWindow.Left = this.Left;
            createGameWindow.Top = this.Top;
            createGameWindow.Show();
            this.Close();
        }

        private void NewGameNet_JoinGame_Button_Click(object sender, RoutedEventArgs e)
        {
            JoinGame joinGameWindow = new JoinGame();

            type = "Join";

            joinGameWindow.Left = this.Left;
            joinGameWindow.Top = this.Top;
            joinGameWindow.Show();
            this.Close();
        }

        private void NewGameNet_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            NewGame NewGame = new NewGame();

            NewGame.Left = this.Left;
            NewGame.Top = this.Top;
            NewGame.Show();
            this.Close();
        }
    }
}
