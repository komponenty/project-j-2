﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers.GUI.NowaGra.NowaGraNet
{
    /// <summary>
    /// Interaction logic for JoinGame.xaml
    /// </summary>
    public partial class JoinGame : Window
    {
        public static String IPAdress;

        public JoinGame()
        {
            InitializeComponent();
        }

        private void JoinGame_JoinGame_Button_Click(object sender, RoutedEventArgs e)
        {
            IPAdress = JoinGame_IPAdress_TextBox.Text;

            GameBoards.GameBoardNet newGameBoardNetWindow = new GameBoards.GameBoardNet();

            newGameBoardNetWindow.Left = this.Left;
            newGameBoardNetWindow.Top = this.Top;
            newGameBoardNetWindow.Show();
            this.Close();
        }

        private void JoinGame_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            NewGameNet NewGameNet = new NewGameNet();

            NewGameNet.Left = this.Left;
            NewGameNet.Top = this.Top;
            NewGameNet.Show();
            this.Close();
        }
    }
}
