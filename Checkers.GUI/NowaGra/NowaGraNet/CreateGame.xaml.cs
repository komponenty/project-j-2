﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.GUIHandler.Impl;
using Checkers.GUIHandler.Contract;

namespace Checkers.GUI.NowaGra.NowaGraNet
{
    /// <summary>
    /// Interaction logic for CreateGame.xaml
    /// </summary>
    public partial class CreateGame : Window
    {
        INetworkMethods networkMethods;
        public static String IPadress;

        public CreateGame()
        {
            InitializeComponent();
            networkMethods = new NetworkMethods();

            foreach (var item in networkMethods.GetLocalIPList())
                if (item.ToString().Count() < 15)
                    CreateGame_AllIPaddress_Combobox.Items.Add(item.ToString());
        }

        private void CreateGame_CreateGame_Button_Click(object sender, RoutedEventArgs e)
        {
            IPadress = CreateGame_AllIPaddress_Combobox.SelectedValue.ToString();

            GameBoards.GameBoardNet newGameBoardNetWindow = new GameBoards.GameBoardNet();

            newGameBoardNetWindow.Left = this.Left;
            newGameBoardNetWindow.Top = this.Top;
            newGameBoardNetWindow.Show();
            this.Close();

        }

        private void CreateGame_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            NewGameNet NewGameNet = new NewGameNet();

            NewGameNet.Left = this.Left;
            NewGameNet.Top = this.Top;
            NewGameNet.Show();
            this.Close();
        }
    }
}
