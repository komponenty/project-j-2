﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.LogIn.Impl;
using Checkers.LogIn.Contract;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for LogInScreen.xaml
    /// </summary>
    public partial class LogInScreen : Window
    {
        public static string userName;
        ILogIn logIn;

        public LogInScreen()
        {
            InitializeComponent();
            logIn = new LogIn.Impl.LogIn();
        }

        private void Login_Button_Connect_Click(object sender, RoutedEventArgs e)
        {
            userName = LogInScreen_Textbox_UserName.Text;

            if (logIn.loggingIn(LogInScreen_Textbox_UserName.Text, LogInScreen_Textbox_Password.Text))
            {
                logIn.close();
                MainWindow mainWindowWindow = new MainWindow();

                mainWindowWindow.Left = this.Left;
                mainWindowWindow.Top = this.Top;
                mainWindowWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Nieprawidłowy login lub hasło");
                logIn.close();
                logIn = new LogIn.Impl.LogIn();
            }
        }

        private void Login_Button_CreatAcc_Click(object sender, RoutedEventArgs e)
        {
            logIn.close();
            CreateAcc createAccWindow = new CreateAcc();

            createAccWindow.Left = this.Left;
            createAccWindow.Top = this.Top;
            createAccWindow.Show();
            this.Close();
        }
    }
}
