﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.GUIHandler.Contract;
using Checkers.GUIHandler.Impl;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for NewGameAI.xaml
    /// </summary>
    public partial class NewGameAI : Window
    {
        public NewGameAI()
        {
            InitializeComponent();

            INetworkMethods methods = new NetworkMethods();

            foreach (var item in methods.GetLocalIPList())
            {
                if (item.ToString().Count() < 15)
                    Chose_IP.Items.Add(item.ToString());
            }
        }

        private void AIGame_Button_START_Click(object sender, RoutedEventArgs e)
        {


        }

        private void AIGame_Button_Back_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Chose_IP.SelectedValue.ToString());
        }
    }
}
