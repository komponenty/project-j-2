﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for LeaderBoard.xaml
    /// </summary>
    public partial class LeaderBoard : Window
    {
        public LeaderBoard()
        {
            InitializeComponent();

            DataSet dataSet = new DataSet();
            dataSet.ReadXml("./dane.xml");

            LeaderBoard_Scores_DataGrid.ItemsSource = dataSet.Tables[0].DefaultView;
        }

        private void LeaderBoard_Buton_Play_Back(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();

            MainWindow.Left = this.Left;
            MainWindow.Top = this.Top;
            MainWindow.Show();
            this.Close();
        }
    }
}
