﻿#pragma checksum "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EC2807D2A0C4FFACA67F91F8F0C4216D"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.34011
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Checkers.GUI.NowaGra.NowaGraAI {
    
    
    /// <summary>
    /// NewGameAI
    /// </summary>
    public partial class NewGameAI : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewGameAI_Button_Play;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton NewGameAI_Radio_Easy;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton NewGameAI_Radio_Hard;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewGameAI_Buton_Back;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Checkers.GUI;component/nowagra/nowagraai/newgameai.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.NewGameAI_Button_Play = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
            this.NewGameAI_Button_Play.Click += new System.Windows.RoutedEventHandler(this.NewGameAI_Button_Play_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.NewGameAI_Radio_Easy = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.NewGameAI_Radio_Hard = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 4:
            this.NewGameAI_Buton_Back = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\..\..\NowaGra\NowaGraAI\NewGameAI.xaml"
            this.NewGameAI_Buton_Back.Click += new System.Windows.RoutedEventHandler(this.NewGameAI_Buton_Play_Back);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

