﻿#pragma checksum "..\..\..\NowaGra\NewGame.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "C9E7ABEB8021D1B28F28FD079E7210C9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Checkers.GUI.NowaGra {
    
    
    /// <summary>
    /// NewGame
    /// </summary>
    public partial class NewGame : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 7 "..\..\..\NowaGra\NewGame.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewGame_NewGameAI_Button;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\NowaGra\NewGame.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewGame_NewGameNet_Button;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\NowaGra\NewGame.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewGame_Buton_Back;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Checkers.GUI;component/nowagra/newgame.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\NowaGra\NewGame.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.NewGame_NewGameAI_Button = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\NowaGra\NewGame.xaml"
            this.NewGame_NewGameAI_Button.Click += new System.Windows.RoutedEventHandler(this.NewGame_NewGameAI_Button_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.NewGame_NewGameNet_Button = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\..\NowaGra\NewGame.xaml"
            this.NewGame_NewGameNet_Button.Click += new System.Windows.RoutedEventHandler(this.NewGame_NewGameNet_Button_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.NewGame_Buton_Back = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\NowaGra\NewGame.xaml"
            this.NewGame_Buton_Back.Click += new System.Windows.RoutedEventHandler(this.NewGame_Buton_Play_Back);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

