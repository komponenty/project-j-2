﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Checkers.LogIn.Contract;
using Checkers.LogIn.Impl;

namespace Checkers.GUI
{
    /// <summary>
    /// Interaction logic for CreateAcc.xaml
    /// </summary>
    public partial class CreateAcc : Window
    {
        ILogIn logIn;

        public CreateAcc()
        {
            InitializeComponent();
            logIn = new LogIn.Impl.LogIn();
        }

        private void CreateAcc_Button_Create_Click(object sender, RoutedEventArgs e)
        {
            if (logIn.createAccount(CreateAcc_TextBox_UserName.Text, CreateAcc_PassBox_Pass.Password))
            {
                MessageBox.Show("Konto zostało utworzone");

                logIn.close();
                LogInScreen logInWindow = new LogInScreen();
                logInWindow.Left = this.Left;
                logInWindow.Top = this.Top;
                logInWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Error");
                logIn = new LogIn.Impl.LogIn();
            }

        }

        private void CreateAcc_Button_Back_Click(object sender, RoutedEventArgs e)
        {
            logIn.close();
            LogInScreen logInWindow = new LogInScreen();
            logInWindow.Left = this.Left;
            logInWindow.Top = this.Top;
            logInWindow.Show();
            this.Close();
        }
    }
}
