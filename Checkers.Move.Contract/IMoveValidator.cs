﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Move.Contract
{
    /// <summary>
    /// Interfejs do Walidatora Ruchów
    /// </summary>
    public interface IMoveValidator
    {
        bool isValidMove(int from, int to);
        bool mayAttack(int pos);
        bool mustAttack();
    }
}
