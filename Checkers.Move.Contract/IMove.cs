﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Move.Contract
{
    /// <summary>
    /// Interfejs do Ruchu
    /// </summary>
    public interface IMove
    {
        int from { get; set; }
        int to { get; set; }
    }
}
