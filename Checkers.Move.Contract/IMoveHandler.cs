﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Move.Contract
{
    /// <summary>
    /// Interfejs do move handler
    /// </summary>
    public interface IMoveHandler
    {
        void clearPiece(int from, int to);
        void move(IList<IMove> moves);
        void move(int from, int to);
        void applyMove(int from, int to);
    }
}
