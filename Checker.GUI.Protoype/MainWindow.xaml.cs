﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Checker.GUI.Protoype
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        

        #region SHOW NEW GAME
        /// <summary>
        /// Show or hide login controls depends on the parameter
        /// </summary>
        /// <param name="show"></param>
        private void ShowNewGame(bool show)
        {
            if (show)
            {
                NewGame_Button_AIGame.Visibility = Visibility.Visible;
                NewGame_Button_NetworkGame.Visibility = Visibility.Visible;
                NewGame_Button_Back.Visibility = Visibility.Visible;
            }
            else
            {
                NewGame_Button_AIGame.Visibility = Visibility.Collapsed;
                NewGame_Button_NetworkGame.Visibility = Visibility.Collapsed;
                NewGame_Button_Back.Visibility = Visibility.Collapsed;
            }
        }
        #endregion


        #region SHOW AI GAME
        /// <summary>
        /// Show or hide login controls depends on the parameter
        /// </summary>
        /// <param name="show"></param>
        private void ShowAIGame(bool show)
        {
            if (show)
            {
                AIGame_Radio_EasyMode.Visibility = Visibility.Visible;
                AIGame_Radio_DefaultMode.Visibility = Visibility.Visible;
                AIGame_Radio_HardMode.Visibility = Visibility.Visible;
                AIGame_Button_Back.Visibility = Visibility.Visible;
                AIGame_Button_START.Visibility = Visibility.Visible;
                AIGame_Combo_MoveTime.Visibility = Visibility.Visible;
                AIGame_Combo_PawnColor.Visibility = Visibility.Visible;
                AIGame_Label_GameMode.Visibility = Visibility.Visible;
                AIGame_Label_MaxMoveTime.Visibility = Visibility.Visible;
                AIGame_Label_PawnColor.Visibility = Visibility.Visible;
            }
            else
            {
                AIGame_Radio_EasyMode.Visibility = Visibility.Collapsed;
                AIGame_Radio_DefaultMode.Visibility = Visibility.Collapsed;
                AIGame_Radio_HardMode.Visibility = Visibility.Collapsed;
                AIGame_Button_Back.Visibility = Visibility.Collapsed;
                AIGame_Button_START.Visibility = Visibility.Collapsed;
                AIGame_Combo_MoveTime.Visibility = Visibility.Collapsed;
                AIGame_Combo_PawnColor.Visibility = Visibility.Collapsed;
                AIGame_Label_GameMode.Visibility = Visibility.Collapsed;
                AIGame_Label_MaxMoveTime.Visibility = Visibility.Collapsed;
                AIGame_Label_PawnColor.Visibility = Visibility.Collapsed;
            }
        }
        #endregion


        


        /// <summary>
        /// Show or hide login controls depends on the parameter
        /// </summary>
        /// <param name="show"></param>
        private void ShowGameBoardAI(bool show)
        {
            if (show)
            {
                CheckersBoardGrid.Visibility = Visibility.Visible;
                GameBoardAI_Label_ShowTime.Visibility = Visibility.Visible;
                GameBoardAI_LAbel_ShowPlayerMovesCounter.Visibility = Visibility.Visible;
                GameBoardAI_Label_ShowAIPawns.Visibility = Visibility.Visible;
                GameBoardAI_Label_ShowPlayerPawns.Visibility = Visibility.Visible;
                GameBoardAI_Button_End.Visibility = Visibility.Visible;
                GameBoardAI_Label_Time.Visibility = Visibility.Visible;
                GameBoardAI_Label_PlayerMovesCounter.Visibility = Visibility.Visible;
                GameBoardAI_Label_AIPawns.Visibility = Visibility.Visible;
                GameBoardAI_Label_PlayerPawns.Visibility = Visibility.Visible;
            }
            else
            {
                CheckersBoardGrid.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_ShowTime.Visibility = Visibility.Collapsed;
                GameBoardAI_LAbel_ShowPlayerMovesCounter.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_ShowAIPawns.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_ShowPlayerPawns.Visibility = Visibility.Collapsed;
                GameBoardAI_Button_End.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_Time.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_PlayerMovesCounter.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_AIPawns.Visibility = Visibility.Collapsed;
                GameBoardAI_Label_PlayerPawns.Visibility = Visibility.Collapsed;
            }
        }


        

        private void NewGame_Button_AIGame_Click(object sender, RoutedEventArgs e)
        {
            ShowAIGame(true);
            
            ShowGameBoardAI(false);
            
        }
    }
}
