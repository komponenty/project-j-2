﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Reflection;

using Checkers.CommServer.Contract;
using System.ServiceModel.Description;

namespace Checkers.CommServer.Impl
{
    /// <summary>
    /// Delegat odpowiadający za liste graczy;
    /// </summary>
    /// <param name="names"></param>
    /// <param name="sender"></param>
    public delegate void ListOfNames(IList<string> names, object sender);

    /// <summary>
    /// Implementacja Serwisu wysyłania
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServerService : ISendServerService
    {
        Dictionary<string, IReceiveServerService> names = new Dictionary<string, IReceiveServerService>();

        IReceiveServerService callback = null;

        public static event ListOfNames ServerListOfNames;
        /// <summary>
        /// Metoda zamykająca serwis
        /// </summary>
        public void Close()
        {
            callback = null;
            names.Clear();
        }
        /// <summary>
        /// Metoda wysyłająca wiadomość
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        public void SendMessage(string msg, string sender, string receiver)
        {
            if (names.ContainsKey(receiver))
            {
                callback = names[receiver];
                callback.ReceiveMessage(msg, sender);
            }
        }
        /// <summary>
        /// Metoda wysyłająca zserializowaną planszę
        /// </summary>
        /// <param name="board"></param>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        public void SendBoard(string board, string sender, string receiver)
        {
            if (names.ContainsKey(receiver))
            {
                callback = names[receiver];
                callback.ReceiveBoard(board, sender);
            }
        }
        /// <summary>
        /// Metoda dodająca Usera do serwisu
        /// </summary>
        /// <param name="name"></param>
        /// <param name="callback"></param>
        private void AddUser(string name, IReceiveServerService callback)
        {
            names.Add(name, callback);
            if (ServerListOfNames != null)
            {
                ServerListOfNames(names.Keys.ToList(), this);
            }
        }
        /// <summary>
        /// Metoda wysyłająca loginy graczy do wszystkich
        /// </summary>
        void SendNamesToAll()
        {
            foreach (KeyValuePair<string, IReceiveServerService> item in names)
            {
                IReceiveServerService proxy = item.Value;
                proxy.SendNames(names.Keys.ToList());
            }

            if (ServerListOfNames != null)
            {
                ServerListOfNames(names.Keys.ToList(), this);
            }
        }
        /// <summary>
        /// Metoda która podpina klienta do serwera
        /// </summary>
        /// <param name="Name"></param>
        public void Start(string Name)
        {
            try
            {
                if (!names.ContainsKey(Name))
                {
                    callback = OperationContext .Current.GetCallbackChannel<IReceiveServerService>();
                    AddUser(Name, callback);
                    SendNamesToAll();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Metoda która odpina klienta od serwera 
        /// </summary>
        /// <param name="Name"></param>
        public void Stop(string Name)
        {
            try
            {
                if (names.ContainsKey(Name))
                {
                    names.Remove(Name);
                    SendNamesToAll();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
