﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.LogIn.Contract;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Checkers.LogIn.Impl
{
    public class LogIn : ILogIn
    {
        Dictionary<String, String> dataBase = new Dictionary<String, String>() { { "admin", "admin" } };
        FileStream fs = new FileStream("DataFile.dat", FileMode.OpenOrCreate);

        public LogIn()
        {
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fs, dataBase);
        }

        public bool createAccount(string inUserName, string inPassword)
        {
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fs, dataBase);
            if (dataBase.ContainsKey(inUserName))
            {
                fs.Close();
                return false;
            }
            else
            {
                dataBase.Add(inUserName, inPassword);
                binaryFormatter.Serialize(fs, dataBase);
                fs.Close();
                return true;
            }
        }

        public bool loggingIn(string inUserName, string inPassword)
        {
            Dictionary<String, String> dataBaseDes = new Dictionary<String, String>();
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fs, dataBase);
            dataBaseDes = (Dictionary<String, String>)binaryFormatter.Deserialize(fs);

            if (!dataBaseDes.ContainsKey(inUserName))
            {
                return false;
            }
            String password = dataBaseDes[inUserName];
            if (password == inPassword)
            {
                fs.Close();
                return true;
            }

            else
            {
                fs.Close();
                return false;
            }
        }

        public void close()
        {
            fs.Close();
        }
    }
}
