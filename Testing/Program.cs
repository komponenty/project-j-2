﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.CheckersBoard.Impl;
using Checkers.Move.Impl;
using Checkers.CheckersBoard.Contract;
using Checkers.Move.Contract;
using Checkers.Computer.Contract;
using Checkers.Computer.Impl;
using System.Diagnostics;
using Ninject;
using Checkers.Infrastructure;
using System.Reflection;
using Checkers.LeaderBoard.Impl;
using Ninject.Parameters;


namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetAssembly(typeof(Configuration)));
            ICheckerBoard board;
            IMoveHandler moveHandler;
            IComputer computer;
            board = kernel.Get<ICheckerBoard>();
            moveHandler = kernel.Get<IMoveHandler>(new ConstructorArgument("board", board));
            computer = kernel.Get<IComputer>(new ConstructorArgument("board", board), new ConstructorArgument("depth", 4));
            Console.ReadKey();
        }
    }
}
