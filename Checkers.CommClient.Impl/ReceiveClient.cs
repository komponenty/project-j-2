﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

using Checkers.CommClient.Contract;
using Checkers.CommClient.Impl;


namespace Checkers.CommClient.Impl
{
    /// <summary>
    /// Delegaty dla odbierania wiadomości i plansz
    /// </summary>
    public delegate void ReceivedMessage(string sender, string message);
    public delegate void ReceivedBoards(string sender, string board);
    public delegate void GotNames(object sender, List<string> names);

    public class ReceiveClient : ServiceServer.ISendServerServiceCallback, IReceiveClient
    {
        public event ReceivedMessage ReceivedMsg;
        public event ReceivedBoards ReceivedBoards;
        public event GotNames NewNames;

        InstanceContext context = null;
        ServiceServer.SendServerServiceClient serverClient = null;
        /// <summary>
        /// Metoada uruchamiająca kompunkikację klient <-> serwer
        /// </summary>
        /// <param name="rc"></param>
        /// <param name="name"></param>
        /// <param name="address"></param>
        public void Start(IReceiveClient rc, string name, string address)
        {
            context = new InstanceContext(rc);
            serverClient = new Checkers.CommClient.Impl.ServiceServer.SendServerServiceClient(context);

            string servicePath = serverClient.Endpoint.ListenUri.AbsolutePath;
            string serviceListenPort = serverClient.Endpoint.Address.Uri.Port.ToString();

            //serverClient.Endpoint.Address = new EndpointAddress("net.tcp://" + address + ":" + serviceListenPort + servicePath);
            serverClient.Endpoint.Address = new EndpointAddress("net.tcp://" + address + ":" + "7886" + "/Server/ServerService/tcp");
            serverClient.Start(name);
        }
        public void Stop(string name)
        {
            serverClient.Stop(name);
        }
        /// <summary>
        /// Implementacja metody wysyłania wiadmomości
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        public void SendMessage(string msg, string sender, string receiver)
        {
            serverClient.SendMessage(msg, sender, receiver);
        }
        /// <summary>
        /// Implementacja metody wysyłania planszy
        /// </summary>
        /// <param name="board"></param>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        public void SendBoard(string board, string sender, string receiver)
        {
            serverClient.SendBoard(board, sender, receiver);
        }
        /// <summary>
        /// Implementacja metody odbierania wiadomości
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="receiver"></param>
        public void ReceiveMessage(string msg, string receiver)
        {
            if (ReceivedMsg != null)
            {
                ReceivedMsg(receiver, msg);
            }
        }
        /// <summary>
        /// Implementacja metody odbierania planszy
        /// </summary>
        /// <param name="board"></param>
        /// <param name="receiver"></param>
        public void ReceiveBoard(string board, string receiver)
        {
            if (ReceivedBoards != null)
            {
                ReceivedBoards(receiver, board);
            }
        }
        /// <summary>
        /// implementacja metody wysyłania planszy
        /// </summary>
        /// <param name="names"></param>
        public void SendNames(string[] names)
        {
            if (NewNames!= null)
            {
                NewNames(this, names.ToList());
            }
        }
    }
}
