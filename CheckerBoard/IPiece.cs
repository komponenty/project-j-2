﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.CheckersBoard.Contract
{
    /// <summary>
    /// Interfejs do pionka
    /// </summary>
    public interface IPiece
    {
        void changeType(PieceType toWhatType);
        void changeColor(PieceColor toWhatColor);
        PieceType _type { get; set; }
        PieceColor _color { get; set; }
    }
}
