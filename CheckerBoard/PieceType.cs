﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.CheckersBoard.Contract
{
    /// <summary>
    /// Enum odpowiadający za typ
    /// </summary>
    public enum PieceType { Empty, Normal, King }
}
