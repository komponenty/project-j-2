﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.CheckersBoard.Contract
{
    /// <summary>
    /// Interfejs do planszy
    /// </summary>
    public interface ICheckerBoard
    {
        int yellowPieces { get; set; }
        int redPieces { get; set; }
        void setCheckerBoard();
        IPiece getPiece(int pos);
        IPiece currentPlayer { get; set; }
        void changeSide();
        int columnRowToTablePos(int column, int row);
        int tablePosToRow(int value);
        int tablePosToColumn(int value);
        ICheckerBoard clone();
        PieceColor winner();
        bool hasEnded();
    } 
}
