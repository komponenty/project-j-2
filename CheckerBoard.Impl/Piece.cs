﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.CheckersBoard.Contract;

namespace Checkers.CheckersBoard.Impl
{
    [Serializable]
    public class Piece : IPiece
    {
        /// <summary>
        /// Definicja właściwości pionka
        /// </summary>
        public PieceType _type { get; set; }
        public PieceColor _color { get; set; }
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="color"></param>
        public Piece(PieceType type, PieceColor color)
        {
            this._type = type;
            this._color = color;
        }
        /// <summary>
        /// Metoda do zmiany typu pionka
        /// </summary>
        /// <param name="toWhatType"></param>
        public void changeType(PieceType toWhatType)
        {
            this._type = toWhatType;
        }       
        /// <summary>
        /// Metoda do zmiany koloru pionka
        /// </summary>
        /// <param name="toWhatColor"></param>
        public void changeColor(PieceColor toWhatColor)
        {
            this._color = toWhatColor;
        }
        /// <summary>
        /// DeepCopy pionka
        /// </summary>
        /// <returns></returns>
        internal IPiece Clone()
        {
            return new Piece(_type, _color);
        }
    }
}
