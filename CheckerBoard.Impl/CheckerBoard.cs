﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Checkers.CheckersBoard.Contract;
using Ninject.Parameters;
using Ninject;
using System.Reflection;

namespace Checkers.CheckersBoard.Impl
{
    [Serializable]
    public class CheckerBoard : ICheckerBoard
    {
        public Piece[] _pieces;
        public IPiece currentPlayer { get; set; }

        public int yellowPieces { get; set; }
        public int redPieces { get; set; }  

        public CheckerBoard()
        {
            this._pieces = new Piece[32];
            this.setCheckerBoard();

        }
        
        /// <summary>
        /// Ustawianie planszy
        /// </summary>
        public void setCheckerBoard()
        {
            currentPlayer = new Piece(PieceType.Empty, PieceColor.Empty);
            

            yellowPieces = 12;
            redPieces = 12;

            for (int i = 0; i < 12; i++)
                this._pieces[i] = new Piece(PieceType.Normal, PieceColor.Red);
            for (int i = 12; i < 20; i++)
                this._pieces[i] = new Piece(PieceType.Empty,PieceColor.Empty);
            for (int i = 20; i < 32; i++)
                this._pieces[i] = new Piece(PieceType.Normal, PieceColor.Yellow);

            this.currentPlayer._color = PieceColor.Yellow; //change
        }
        /// <summary>
        /// Zwraca pionek z zadanej pozycji
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public IPiece getPiece(int pos)
        {
            if (pos < 0 || pos > 32)
                throw new Exception("getPiece poza zakresem");
            else
                return this._pieces[pos];
        }
        /// <summary>
        /// Zmienia aktywnego gracza
        /// </summary>
        public void changeSide()
        {
            if (this.currentPlayer._color == PieceColor.Yellow)
                this.currentPlayer._color = PieceColor.Red;
            else
                this.currentPlayer._color = PieceColor.Yellow;
        }
        /// <summary>
        /// Konwersja współrzędnych x i y do pozycji w tabeli
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public int columnRowToTablePos(int column, int row)
        {
            if (row % 2 == 0)
                return row * 4 + (column - 1) / 2;
            else
                return row * 4 + column / 2;
        }
        /// <summary>
        /// Konwersja pozycji w tabeli do x
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int tablePosToRow(int value)
        {
            return value / 4;
        }
        /// <summary>
        /// Konwersja pozycji w tabeli do y
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int tablePosToColumn(int value)
        {
            return (value % 4) * 2 + ((value / 4) % 2 == 0 ? 1 : 0);
        }

        public ICheckerBoard clone()
        {
            CheckerBoard board = new CheckerBoard();
            board.currentPlayer._color = this.currentPlayer._color;

            for (int i = 0; i < 32; i++)
                board._pieces[i] = (Piece)this._pieces[i].Clone();

            return board;
        }
       
        public PieceColor winner()
        {
            if (yellowPieces == 0)
                return PieceColor.Red;
            else if (redPieces == 0)
                return PieceColor.Yellow;
            else
                return PieceColor.Empty;
        }

        public bool hasEnded()
        {
            return this.yellowPieces == 0 || this.redPieces == 0;
        }
    }
}
